package com.example.aicha.votesvel;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.aicha.votesvel.helper.CheckNetworkStatus;
import com.example.aicha.votesvel.helper.ConfigHelper;
import com.example.aicha.votesvel.helper.MySingleton;
import com.example.aicha.votesvel.helper.SessionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity  {
    private static final String KEY_EMPTY = "";
    private static final String KEY_DATA = "data";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_SUCCESS = "success";
    private static final String KEY_ID_USER = "idUtilisteur";
    private static final String KEY_LOGIN = "login";
    private static final String KEY_PWD = "pwd";
    private static final String KEY_TELEPHONE = "telephone";
    private static final String KEY_NAME = "name";
    private static final String KEY_ROLE= "role";
    private static final String KEY_ID_BUREAU= "id_bureau";
    private static final String KEY_BUREAU= "bureau";
    private static final String KEY_CENTRE= "centre";

    String API_URL;
    String BASE_URL;

    private String idUtilisteur,login,pwd,telephone,name,role,id_bureau,bureau,centre,username,password;

    private int success;
    private String message;
    private ProgressDialog pDialog;

    private Button btnConnexion;
    EditText edtLogin, edtPwd;

    private SessionHandler session;
    private Intent i;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        API_URL = ConfigHelper.getConfigValue(this,"api_url");
        BASE_URL = API_URL+"getUser.php";

        setTitle("Authentification - SVEL");

        session = new SessionHandler(getApplicationContext());

        edtLogin=findViewById(R.id.login);
        edtPwd=findViewById(R.id.password);

        btnConnexion=findViewById(R.id.btbcon);
        btnConnexion.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                //Retrieve the data entered in the edit texts
                username = edtLogin.getText().toString().toLowerCase().trim();
                password = edtPwd.getText().toString().trim();
                if (validateInputs()) {
                    if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {
                        login_connexion();
                    }
                    else {
                        Toast.makeText(LoginActivity.this,
                                "Impossible de se connecter à l'internet",
                                Toast.LENGTH_LONG).show();

                    }

                }

            }
        });


}


    /**
     * Launch Dashboard Activity on Successful Login
     */
    private void loadDashboard() {
        i = new Intent(getApplicationContext(), AdminActivity.class);
        startActivity(i);
        finish();

    }
    private void loadConnaitreBureau() {
        i = new Intent(getApplicationContext(), ConnaitreBureauActivity.class);
        startActivity(i);
        finish();

    }

    private void loadAccueilChefCentre() {
        i = new Intent(getApplicationContext(), CentreBureauActivity.class);
        startActivity(i);
        finish();

    }
 private void loadAccueilAdmin() {
        i = new Intent(getApplicationContext(), ResultatParCentreActivity.class);
        startActivity(i);
        finish();

    }

    /**
     * Display Progress bar while Logging in
     */

    private void displayLoader() {
        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("Connexion en cours...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }


    private void login_connexion() {
        displayLoader();
        JSONObject request = new JSONObject();
        try {
            //Populate the request parameters
            request.put(KEY_LOGIN, username);
            request.put(KEY_PWD, password);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsArrayRequest = new JsonObjectRequest
                (Request.Method.POST, BASE_URL, request, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.dismiss();
                        try {
                            //Check if master got logged in successfully

                            if (response.getInt(KEY_SUCCESS) == 1) {
                                JSONObject jsonObject=response.getJSONObject(KEY_DATA);
//                              idUtilisteur,login,pwd,telephone,name,role,id_bureau,bureau
                                idUtilisteur = jsonObject.getString("idUtilisteur");
                                login = jsonObject.getString("login");
                                pwd = jsonObject.getString("pwd");
                                telephone = jsonObject.getString("telephone");
                                name = jsonObject.getString("name");
                                role = jsonObject.getString("role");
                                id_bureau = jsonObject.getString("id_bureau");
                                bureau = jsonObject.getString("bureau");
                                centre = jsonObject.getString("centre");

                                Log.d("SESSION","idUtilisteur: "+idUtilisteur);
                                Log.d("SESSION","login : "+login);
                                Log.d("SESSION","pwd : "+pwd);
                                Log.d("SESSION","telephone : "+telephone);
                                Log.d("SESSION","name : "+name);
                                Log.d("SESSION","role : "+role);
                                Log.d("SESSION","id_bureau : "+id_bureau);
                                Log.d("SESSION","bureau : "+bureau);
                                Log.d("SESSION","centre : "+centre);


                                session.loginUser(idUtilisteur,login,pwd,telephone,name,role,id_bureau,bureau,centre);

                                Toast.makeText(getApplicationContext(),response.getString(KEY_MESSAGE), Toast.LENGTH_LONG).show();

                                if(role.equals("RECEPTIONNISTE")){
                                    loadConnaitreBureau();
                                }else if(role.equals("PRESIDENTB")){
                                    loadDashboard();
                                }else if(role.equals("CHEFCENTRE")){
                                    loadAccueilChefCentre();
                                }else if(role.equals("ADMIN")){
                                    loadAccueilAdmin();
                                }


                            }else{
                                Toast.makeText(getApplicationContext(),response.getString(KEY_MESSAGE), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();

                        //Display error message whenever an error occurs
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsArrayRequest);
    }


    private boolean validateInputs() {
        if(KEY_EMPTY.equals(edtLogin.getText().toString())){
            edtLogin.setError("Nom d'utilisateur obligatoire");
            edtLogin.requestFocus();
            return false;
        }

        if(KEY_EMPTY.equals(edtPwd.getText().toString())){
            edtPwd.setError("Mot de passe obligatoire");
            edtPwd.requestFocus();
            return false;
        }
        return true;
    }

}

