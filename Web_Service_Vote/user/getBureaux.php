<?php
/**
 * Created by PhpStorm.
 * User: AICHA
 * Date: 01/01/2018
 * Time: 03:30
 */

include '../db/db_connect.php';
//Query to select movie id and movie name
$result = array();
$electeurArray = array();
$response = array();

//Get the input request parameters

//Check for mandatory parameter
if(isset($_GET['centre'])){

    $centre =$_GET['centre'];


    $query = "SELECt bureau.id as id_bureau, bureau.nom_bureau as nom_bureau
    FROM bureau,centre
    WHERE bureau.centre_id=centre.id AND centre.nom_centre='$centre' ORDER BY bureau.id";

    if($stmt=$con->prepare($query)){
//        $stmt->bind_param("ss",$centre,$bureau);
        $stmt->execute();
        $stmt->bind_result($id_bureau,$nom_bureau);

        while($stmt->fetch()){
            //Populate the movie array
            $electeurArray["id_bureau"] = $id_bureau;
            $electeurArray["nom_bureau"] = $nom_bureau;

            $result[]=$electeurArray;

        }

        $response["success"] = 1;
        $response["data"] = $result;
        $response["message"] = "En cours de Listing";
        $stmt->close();


    }else{
        //Some error while fetching data
        $response["success"] = 0;
        $response["data"] = $result;
        $response["message"] ="Erreur de serveur";

    }


}else{
    //When the mandatory parameter movie_id is missing
    $response["status"] = 0;
    $response["data"] = $result;
    $response["message"] = "Parametre manquant";

}
//Display JSON response
echo json_encode($response);
//var_dump($response);
?>