package com.example.hp.svelmobile;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp.svelmobile.imageLoader.ImageLoader;

import java.io.InputStream;

public class GridViewAdapter  extends BaseAdapter {

    private Context context;
   final  private String [] app_name;
   final  private String [] app_partie;
   final  private String [] app_photo;
    private int [] app_icon;
//    ImageLoader imgLoader;
//    private InputStream[] app_input;

//    public GridViewAdapter(Context context, String [] app_name,String [] app_partie, InputStream[] app_input) {
    public GridViewAdapter(Context context, String [] app_name,String [] app_partie, String [] app_photo) {

        this.context=context;
        this.app_photo=app_photo;
        this.app_name=app_name;
        this.app_partie=app_partie;
//        imgLoader = new ImageLoader(context.getApplicationContext());
    }

    @Override
    public int getCount() {
        return app_name.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView=inflater.inflate(R.layout.row_grid, null);


        final TextView name=(TextView)convertView.findViewById(R.id.textView1);
        name.setText(app_name[position]);
        final TextView partie=(TextView)convertView.findViewById(R.id.textView);
        partie.setText(app_partie[position]);

      //  ImageView icon=convertView.findViewById(R.id.imageView);
//        icon.setImageResource(app_icon[position]);

        // Loader image - will be shown before loading image
//        int loader = R.drawable.loader;

        // Imageview to show
        ImageView image =convertView.findViewById(R.id.imageView);
        // ImageLoader class instance


//        if(position==0){
//            icon.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            icon.setAdjustViewBounds(true);
//            icon.setImageBitmap(BitmapFactory.decodeStream(app_input[0]));



            // whenever you want to load an image from url
            // call DisplayImage function
            // url - image url to load
            // loader - loader image, will be displayed before getting image
            // image - ImageView
//            imgLoader.DisplayImage(app_photo[position], loader, image);
            Log.d("imgLoader","imgLoader : "+app_photo[position]);
        new DownloadImageTask((ImageView)convertView.findViewById(R.id.imageView))
                .execute(app_photo[position]);

//        }else{
//            icon.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            icon.setAdjustViewBounds(true);
//            icon.setImageBitmap(BitmapFactory.decodeStream(app_input[position]));
//            imgLoader.DisplayImage(app_photo[position], loader, image);
//        }

//        Log.d("position","position : "+position+" "+app_input[position]);


        return convertView;
    }

//    InputStream is; //stream pointing to your blob or file
//    imageView=new ImageView(this);
//    imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
//    imageView.setAdjustViewBounds(true);
//    imageView.setImageBitmap(BitmapFactory.decodeStream(is));


//    new DownloadImageTask((ImageView) findViewById(R.id.imageView1)) .execute(MY_URL_STRING);

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
