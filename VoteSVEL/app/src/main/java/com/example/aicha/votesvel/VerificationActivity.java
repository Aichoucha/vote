package com.example.aicha.votesvel;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.aicha.votesvel.helper.CheckNetworkStatus;
import com.example.aicha.votesvel.helper.ConfigHelper;
import com.example.aicha.votesvel.helper.HttpJsonParser;
import com.example.aicha.votesvel.helper.MySingleton;
import com.example.aicha.votesvel.helper.SessionHandler;
import com.example.aicha.votesvel.helper.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class VerificationActivity extends AppCompatActivity {

    private static final String KEY_EMPTY = "";

    private static final String KEY_DATA = "data";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_SUCCESS = "success";
    private static final String KEY_ID_ELECTEUR = "id_Electeur";
    private static final String KEY_NUMELECTEUR = "numElecteur";
    private static final String KEY_NUMMATRICULE = "numMatricule";
    private static final String KEY_NOM = "nom";
    private static final String KEY_PRENOM = "prenom";
    private static final String KEY_DATE_NAISSANCE= "date_naissance";
    private static final String KEY_LIEU_NAISSANCE= "lieu_naissance";
    private static final String KEY_PAYS_NATIONALITE= "pays_nationalite";
    private static final String KEY_PARCOURS= "parcours";
    private static final String KEY_STATUT= "statut";
    private static final String KEY_GENRE= "genre";
    private static final String KEY_ETAT= "etat";

    String BASE_URL;
    String API_URL;
    String BASE_URL2;

    private String id_Electeur;
    private String numElecteur;
    private String numMatricule;
    private String id_bureau;
    private String nom;
    private String prenom;
    private String date_naissance;
    private String lieu_naissance;
    private String pays_nationalite;
    private String parcours;
    private String statut;
    private String genre;
    private String etat;
    private String role;
    private String bureau;
    private String centre;

    private TextView txtElecteur,txtInfo;
    EditText edtNumElecteur;
    private Button btnVerifier,btnAnnuler,btnAutoriser,button;

    private int success;
    private String message,txtNumElecteur,txtElecteurMessage;
    private ProgressDialog pDialog;

    private SessionHandler session;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        API_URL = ConfigHelper.getConfigValue(this,"api_url");
        BASE_URL2 = API_URL+"autoriseAVoter.php";


        session = new SessionHandler(getApplicationContext());
        user=session.getUserDetails();
        role=user.getRole();

        if(role.equals("RECEPTIONNISTE")){
            BASE_URL = API_URL+"findElecteurReceptionniste.php";
        }else if(role.equals("PRESIDENTB")) {
            BASE_URL = API_URL+"findElecteur.php";
        }

        setTitle("Vérification - SVEL | "+user.getName());

        txtElecteur=findViewById(R.id.txtElecteur);

        edtNumElecteur=findViewById(R.id.edtNum);


        btnAutoriser=findViewById(R.id.btnAutoriser);

        btnVerifier=findViewById(R.id.btnVerifier);
        button=findViewById(R.id.button);

        btnVerifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNumElecteur=edtNumElecteur.getText().toString().trim();

                    if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {

                        if(validateInputs()){
                            loadElecteur();
                        }

                    }
                    else {
                        Toast.makeText(VerificationActivity.this,
                                "Impossible de se connecter à l'internet",
                                Toast.LENGTH_LONG).show();

                    }


            }
        });
        btnAnnuler=findViewById(R.id.btnAnnuler);
        btnAnnuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnVerifier.setVisibility(View.VISIBLE);
                btnAutoriser.setVisibility(View.GONE);

                edtNumElecteur.setText("");
                txtElecteur.setText("");
            }
        });

        btnAutoriser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {


                    new android.support.v7.app.AlertDialog.Builder(VerificationActivity.this)
                            .setTitle("Vous allez autoriser un vote")
                            .setMessage("êtes-vous sûr ?")
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener() {
                                @RequiresApi(api = Build.VERSION_CODES.M)
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    new VerificationActivity.AddEtatVoteAsyncTask().execute();
                                }
                            })
                            .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();
                }
                else {
                    Toast.makeText(VerificationActivity.this,
                            "Impossible de se connecter à l'internet",
                            Toast.LENGTH_LONG).show();

                }
            }
        });
    }

    private class AddEtatVoteAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display proggress bar
            pDialog = new ProgressDialog(VerificationActivity.this);
            pDialog.setMessage("Autorisation en cours...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpJsonParser httpJsonParser = new HttpJsonParser();
            Map<String, String> httpParams = new HashMap<>();
            //Populating request parameters
            httpParams.put(KEY_NUMELECTEUR, txtNumElecteur);
            httpParams.put("bureau", user.getBureau());
            httpParams.put("centre", user.getCentre());
            httpParams.put("mmesamake", "AICHA DEMBELE");

            Log.d("KEY_NUMELECTEUR",KEY_NUMELECTEUR+" : "+txtNumElecteur);

            JSONObject jsonObject = httpJsonParser.makeHttpRequest(BASE_URL2, "POST", httpParams);

            try {
                success = jsonObject.getInt(KEY_SUCCESS);
                message = jsonObject.getString(KEY_MESSAGE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    if (success == 1) {
                        txtElecteur.setText(message);
                        txtElecteur.setTextColor(Color.GRAY);
                        btnAutoriser.setVisibility(View.GONE);
                        //Display success message
//                        Toast.makeText(VerificationActivity.this,message, Toast.LENGTH_LONG).show();
//                        Intent i = getIntent();
//                        //send result code 20 to notify about movie update
//                        setResult(20, i);
//                        //Finish ths activity and go back to listing activity
//                        finish();

                    } else {
                        txtElecteur.setText(message);
                        txtElecteur.setTextColor(Color.RED);
                        Toast.makeText(VerificationActivity.this,message, Toast.LENGTH_LONG).show();

                    }
                }
            });
        }
    }


    private void displayLoader() {
        pDialog = new ProgressDialog(VerificationActivity.this);
        pDialog.setMessage("Vérification en cours...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void loadElecteur(){
        displayLoader();
        JSONObject request = new JSONObject();
       try {
           //Populate the request parameters
            request.put(KEY_NUMELECTEUR, txtNumElecteur);
           request.put("bureau", user.getBureau());
           request.put("centre", user.getCentre());
           request.put("id_bureau", user.getId_bureau());
           Log.d("Veirification ELECTEUR","Numero electeur : "+txtNumElecteur);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsArrayRequest = new JsonObjectRequest
                (Request.Method.POST, BASE_URL, request, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.dismiss();
                        try {
                            //Check if master got logged in successfully
                            message=response.getString(KEY_MESSAGE);
                            Log.d("Veirification ELECTEUR","Verification electeur : "+message);

                            if (response.getInt(KEY_SUCCESS) == 1) {
                                JSONObject jsonObject=response.getJSONObject(KEY_DATA);

                                id_Electeur = jsonObject.getString("id_Electeur");
                                numElecteur = jsonObject.getString("numElecteur");
                                numMatricule = jsonObject.getString("numMatricule");
                                nom = jsonObject.getString("nom");
                                prenom = jsonObject.getString("prenom");
                                date_naissance = jsonObject.getString("date_naissance");
                                lieu_naissance = jsonObject.getString("lieu_naissance");
                                pays_nationalite = jsonObject.getString("pays_nationalite");
                                parcours = jsonObject.getString("parcours");
                                statut = jsonObject.getString("statut");
                                genre = jsonObject.getString("genre");
                                etat = jsonObject.getString("etat");
                                id_bureau = jsonObject.getString("id_bureau");
                                bureau = jsonObject.getString("bureau");
                                centre = jsonObject.getString("centre");

                                button.setText(centre+" : "+bureau);

                                if(role.equals("RECEPTIONNISTE")){

                                    txtElecteur.setText(message);
                                    txtElecteur.setTextColor(Color.BLACK);
                                    btnVerifier.setVisibility(View.VISIBLE);
                                    btnAutoriser.setVisibility(View.GONE);

                                }else if(role.equals("PRESIDENTB")){

                                    if(etat.equals("PAS VOTER")){
                                        txtElecteur.setText(message);
                                        txtElecteur.setTextColor(Color.BLACK);
                                        btnVerifier.setVisibility(View.GONE);
                                        btnAutoriser.setVisibility(View.VISIBLE);
                                    }else if(etat.equals("VEUT VOTER")){
                                        txtElecteur.setText("Déjà autorisé : "+message);
                                        txtElecteur.setTextColor(Color.RED);
                                        btnVerifier.setVisibility(View.GONE);
                                        btnAutoriser.setVisibility(View.GONE);
                                    }else if(etat.equals("A VOTE")){
                                        txtElecteur.setText("A déjà voté");
                                        txtElecteur.setTextColor(Color.RED);
                                        btnVerifier.setVisibility(View.GONE);
                                        btnAutoriser.setVisibility(View.GONE);
                                    }

                                }else{

                                }





                                Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();



                            }else{
                                btnVerifier.setVisibility(View.VISIBLE);
                                btnAutoriser.setVisibility(View.GONE);

                                txtElecteur.setText(message);
                                txtElecteur.setTextColor(Color.RED);
//                                txtElecteur.setText("Désolé ! Une erreur s'est produite !");
                                Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();

                        //Display error message whenever an error occurs
                        Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsArrayRequest);
    }

    private boolean validateInputs() {
        if(KEY_EMPTY.equals(edtNumElecteur.getText().toString())){
            edtNumElecteur.setError("Numéro d'électeur obligatoire svp !");
            edtNumElecteur.requestFocus();
            return false;
        }
        return true;
    }


}
