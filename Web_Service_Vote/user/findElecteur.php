<?php

$userArray = array();
$response = array();

include '../db/db_connect.php';
include '../db/fonctions.php';
//Get the input request parameters
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array


//if( isset($input['numElecteur'])&& isset($input['id_bureau']) ){
if( isset($input['numElecteur']) && isset($input['centre'])&& isset($input['bureau']) ){

//    $id_bureau=$input['id_bureau'];
    
$num = $input['numElecteur'];
$centre = $input['centre'];
$bureau = $input['bureau'];



$query = "SELECT e.id_Electeur as id_Electeur ,e.numElecteur as numElecteur,e.numMatricule as numMatricule ,e.nom as nom,e.prenom as prenom ,e.date_naissance as date_naissance,e.lieu_naissance as lieu_naissance,e.pays_nationalite as pays_nationalite,e.parcours as parcours,e.statut as statut,e.genre as genre,e.etat as etat,e.id_bureau as id_bureau,b.nom_bureau as bureau,c.nom_centre as centre
FROM electeur e,bureau b, centre c
WHERE e.id_bureau=b.id AND b.centre_id=c.id AND numElecteur=? AND c.nom_centre=? AND b.nom_bureau=?";
//WHERE e.id_bureau=b.id AND b.centre_id=c.id AND id_bureau=? AND numElecteur=?";
if($stmt = $con->prepare($query)){
    //Bind movie_id parameter to the query
//    $stmt->bind_param("ss",$id_bureau,$num);
    $stmt->bind_param("sss",$num,$centre,$bureau);
    $stmt->execute();
    //Bind fetched result to variables id,reference,name,telephone,email,password,image,etat,role,date_adhesion
    $stmt->bind_result($id_Electeur,$numElecteur,$numMatricule,$nom,$prenom,$date_naissance,$lieu_naissance,$pays_nationalite,$parcours,$statut,$genre,$etat,$id_bureau,$bureau,$centre);
    //Check for results

    if($stmt->fetch()){
        //Populate the movie array
        $userArray["id_Electeur"] = $id_Electeur;
        $userArray["numElecteur"] = $numElecteur;
        $userArray["numMatricule"] = $numMatricule;
        $userArray["nom"] = $nom;
        $userArray["prenom"] = $prenom;
        $date=format_date($date_naissance);
        $userArray["date_naissance"] = $date;
        $userArray["lieu_naissance"] = $lieu_naissance;
        $userArray["pays_nationalite"] = $pays_nationalite;
        $userArray["parcours"] = $parcours;
        $userArray["statut"] = $statut;
        $userArray["genre"] = $genre;
        $userArray["etat"] = $etat;
        $userArray["id_bureau"] = $id_bureau;
        $userArray["bureau"] = $bureau;
        $userArray["centre"] = $centre;
            if($etat==="A VOTER"){
                $success=0;
                $message="Le numero d'électeur $numElecteur de $prenom $nom né(e) le $date à $lieu_naissance  a déja voté";

               
            }else{
                $success=1;
                $message="Le numero d'électeur $numElecteur de $prenom $nom né(e) le $date à $lieu_naissance  n'a pas encore voté";

            }
//        est du «Bureau II» du centre de vote « FST »
        $response["success"] = $success;
        $response["message"] =$message;
        $response["data"] = $userArray;

    }else{
        //When movie is not found
         $response["success"] = 0;
         $response["message"] = "Cet électeur n'est pas dans ton bureau!";
         $response["data"] = $userArray;
    }
    $stmt->close();


}else{
    //Whe some error occurs
    $response["success"] = 0;
    $response["message"] = "Erreur de système";
    $response["data"] = $userArray;

}
}else{
    //When the mandatory parameter movie_id is missing
    $response["status"] = 0;
    $response["message"] = "Parametre manquant";
    $response["data"] = $userArray;
}

//Display JSON response
echo json_encode($response);
?>