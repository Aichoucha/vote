<?php

$userArray = array();
$response = array();

include '../db/db_connect.php';
include '../db/fonctions.php';

if( isset($_GET['numElecteur'])){ 
    
$num = $_GET['numElecteur'];
    
$query = "SELECT * FROM electeur WHERE numElecteur=?";
if($stmt = $con->prepare($query)){
    //Bind movie_id parameter to the query
    $stmt->bind_param("s",$num);
    $stmt->execute();
    //Bind fetched result to variables id,reference,name,telephone,email,password,image,etat,role,date_adhesion
    $stmt->bind_result($id_Electeur,$numElecteur,$numMatricule,$nom,$prenom,$date_naissance,$lieu_naissance,$pays_nationalite,$parcours,$statut,$genre,$etat,$id_bureau);
    //Check for results

    if($stmt->fetch()){
        //Populate the movie array
        $userArray["id_Electeur"] = $id_Electeur;
        $userArray["numElecteur"] = $numElecteur;
        $userArray["numMatricule"] = $numMatricule;
        $userArray["nom"] = $nom;
        $userArray["prenom"] = $prenom;
        $userArray["date_naissance"] = format_date($date_naissance);
        $userArray["lieu_naissance"] = $lieu_naissance;
        $userArray["pays_nationalite"] = $pays_nationalite;
        $userArray["parcours"] = $parcours;
        $userArray["statut"] = $statut;
        $userArray["genre"] = $genre;
        $userArray["etat"] = $etat;
        $userArray["id_bureau"] = $id_bureau;

        $response["success"] = 1;
        $response["message"] ="";
        $response["data"] = $userArray;

    }else{
        //When movie is not found
         $response["success"] = 0;
         $response["message"] = "Electeur invalide";
         $response["data"] = $userArray;
    }
    $stmt->close();


}else{
    //Whe some error occurs
    $response["success"] = 0;
    $response["message"] = "Erreur de système";
    $response["data"] = $userArray;

}
}else{
    //When the mandatory parameter movie_id is missing
    $response["status"] = 0;
    $response["message"] = "Parametre manquant";
    $response["data"] = $userArray;
}

//Display JSON response
echo json_encode($response);
?>