<?php
  
function getNbElecteurs($centre,$bureau){
    $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);      
    $query = "SELECT count(e.id_Electeur) as nb_electeur
              FROM electeur e, bureau b ,centre c
              WHERE e.id_bureau=b.id  and b.centre_id=c.id AND c.nom_centre=? AND b.nom_bureau=? GROUP BY e.id_bureau";
    if($stmt = $con->prepare($query)){
        //Bind movie_id parameter to the query
        $stmt->bind_param("ss",$centre,$bureau);
        $stmt->execute();
        $stmt->bind_result($nb_electeur);
        if($stmt->fetch()){
            $id=$nb_electeur;
            
        }
        $stmt->close();
    }
    return $id;
   }


function getNontVotants($centre,$bureau){
    $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);
    $query =  "SELECT count(e.id_Electeur) as nb_votant
    FROM electeur e, bureau b ,centre c
    WHERE e.id_bureau=b.id  and b.centre_id=c.id AND e.etat='PAS VOTER' AND c.nom_centre=? AND b.nom_bureau=? GROUP BY e.id_bureau";
    if($stmt = $con->prepare($query)){
        //Bind movie_id parameter to the query
        $stmt->bind_param("ss",$centre,$bureau);
        $stmt->execute();
        $stmt->bind_result($nb_votant);
        if($stmt->fetch()){
            $id=$nb_votant;

        }
        $stmt->close();
    }
    return $id;
}


function id_Candidat($sigle_parti){
    $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);
    $query = "SELECT id_Candidat FROM candidat WHERE sigle_parti=?";
    if($stmt = $con->prepare($query)){
        //Bind movie_id parameter to the query
        $stmt->bind_param("s",$sigle_parti);
        $stmt->execute();
        $stmt->bind_result($id_Candidat);
        if($stmt->fetch()){
            $id=$id_Candidat;

        }
        $stmt->close();
    }
    return $id;
}
   
function electeur_aVoter($id_Electeur){ 
    $bool=false;
    $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);      
    $query = "UPDATE electeur SET etat='A VOTE' WHERE id_Electeur=?";
    if($stmt = $con->prepare($query)){
        //Bind movie_id parameter to the query
        $stmt->bind_param("s",$id_Electeur);
        $stmt->execute();
        if($stmt->affected_rows == 1){
            $bool=true;
        }
        
        $stmt->close();
    }
    return $bool;
   }

    function allCandidats(){
		
        $result = array();
        $electeurArray = array();

        $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);
        $query = "SELECT c.id_Candidat as id_Candidat, c.nom as nom, c.prenom as prenom,c.genre as genre, c.sigle_parti as sigle_parti, c.photo_Candidat as photo_Candidat FROM candidat c ORDER BY c.sigle_parti";
        if($stmt=$con->prepare($query)){
            $stmt->execute();
            $stmt->bind_result($id_Candidat,$nom,$prenom,$genre,$sigle_parti,$photo_Candidat);
            while($stmt->fetch()){
                //Populate the movie array
                $electeurArray["id_Candidat"] = $id_Candidat;
                $electeurArray["nom"] = $nom;
                $electeurArray["prenom"] = $prenom;
                $electeurArray["genre"] = $genre;
                $electeurArray["sigle_parti"] = $sigle_parti;

                $electeurArray["photo"] = "http://10.0.2.2/Web_Service_Vote/img/".$photo_Candidat;
                //  $electeurArray["photo"] = "http://www.maliafrique.ml/Web_Service_Vote/img/".$photo_Candidat;
				
                //  $electeurArray["photo"] = "http://www.maliafrique.ml/Web_Service_Vote/img/".$photo_Candidat;
    //            $electeurArray["photo_Candidat"] = '<img src="data:image/jpeg;base64,'.base64_encode($photo_Candidat).'"/>';
    //            $electeurArray["photo_Candidat"] = base64_encode($photo_Candidat);

                $result[]=$electeurArray;

            }
            $stmt->close();
    }

    return $result;
}

function verifierSiElecteurVeutVoter($centre,$bureau){
        $veutVoter=false;
        $etat = "VEUT VOTER";
        $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);
        $query = "SELECT e.id_Electeur as id_Electeur ,e.numElecteur as numElecteur,e.numMatricule as numMatricule ,e.nom as nom,e.prenom as prenom ,e.date_naissance as date_naissance,e.lieu_naissance as lieu_naissance,e.pays_nationalite as pays_nationalite,e.parcours as parcours,e.statut as statut,e.genre as genre,e.etat as etat,e.id_bureau as id_bureau,b.nom_bureau as bureau,c.nom_centre as centre
        FROM electeur e,bureau b, centre c
        WHERE e.id_bureau=b.id AND b.centre_id=c.id AND etat=? AND c.nom_centre=? AND b.nom_bureau=?";
        if($stmt = $con->prepare($query)){
            $stmt->bind_param("sss",$etat,$centre,$bureau);
            $stmt->execute();
            $stmt->bind_result($id_Electeur,$numElecteur,$numMatricule,$nom,$prenom,$date_naissance,$lieu_naissance,$pays_nationalite,$parcours,$statut,$genre,$etat,$id_bureau,$bureau,$centre);

             if($stmt->fetch()){
                $veutVoter=true;
            }
            $stmt->close();
        }
        return $veutVoter;
}


   
function format_date($original='', $format="%d-%m-%Y") { 
    $format = ($format=='date' ? "%m-%d-%Y" : $format); 
    $format = ($format=='datetime' ? "%m-%d-%Y %H:%M:%S" : $format); 
    $format = ($format=='mysql-date' ? "%Y-%m-%d" : $format); 
    $format = ($format=='mysql-datetime' ? "%Y-%m-%d %H:%M:%S" : $format); 
    return (!empty($original) ? strftime($format, strtotime($original)) : "" );
    
}

function format_date_time($original='', $format="%d-%m-%Y %H:%M:%S") { 
    $format = ($format=='date' ? "%m-%d-%Y" : $format); 
    $format = ($format=='datetime' ? "%m-%d-%Y %H:%M:%S" : $format); 
    $format = ($format=='mysql-date' ? "%Y-%m-%d" : $format); 
    $format = ($format=='mysql-datetime' ? "%Y-%m-%d %H:%M:%S" : $format); 
    return (!empty($original) ? strftime($format, strtotime($original)) : "" );
    
}




      ?>
   