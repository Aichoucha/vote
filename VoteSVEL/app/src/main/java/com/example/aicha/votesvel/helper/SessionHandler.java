package com.example.aicha.votesvel.helper;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Date;

public class SessionHandler {
//    idUtilisteur,login,pwd,telephone,name,role,id_bureau,bureau
    private static final String PREF_NAME = "UserSession";
    private static final String KEY_IDUSER = "idUtilisteur";
    private static final String KEY_LOGIN = "login";
    private static final String KEY_PWD = "pwd";
    private static final String KEY_TELEPHONE = "telephone";
    private static final String KEY_NAME = "name";
    private static final String KEY_ROLE = "role";
    private static final String KEY_ID_BUREAU = "id_bureau";
    private static final String KEY_BUREAU = "bureau";
    private static final String KEY_CENTRE = "centre";

    private static final String KEY_EMPTY = "";

    private static final String KEY_EXPIRES = "expires";

    Date sessionExpiryDate;


    private Context mContext;
    private SharedPreferences.Editor mEditor;
    private SharedPreferences mPreferences;


    public SessionHandler(Context mContext) {
        this.mContext = mContext;
        mPreferences = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        this.mEditor = mPreferences.edit();
    }

    /**
    * Logs in the master by saving master details and setting session
     *
     * @param name
     * @param telephone
     */
    public void loginUser(String idUtilisteur,String login,String pwd,String telephone,String name,String role,String id_bureau,String bureau,String centre) {
        mEditor.putString(KEY_IDUSER, idUtilisteur);
        mEditor.putString(KEY_LOGIN, login);
        mEditor.putString(KEY_PWD, pwd);
        mEditor.putString(KEY_TELEPHONE, telephone);
        mEditor.putString(KEY_NAME, name);
        mEditor.putString(KEY_ROLE, role);
        mEditor.putString(KEY_ID_BUREAU, id_bureau);
        mEditor.putString(KEY_BUREAU, bureau);
        mEditor.putString(KEY_CENTRE, centre);
        Date date = new Date();

        //Set master session for next 7 days
        long millis = date.getTime() + (7 * 24 * 60 * 60 * 1000);
        mEditor.putLong(KEY_EXPIRES, millis);
        mEditor.commit();
    }



    /**
     * Checks whether master is logged in
     *
     * @return
     */
    public boolean isLoggedIn() {
        Date currentDate = new Date();

        long millis = mPreferences.getLong(KEY_EXPIRES, 0);

        /* If shared preferences does not have a value
         then master is not logged in
         */
        if (millis == 0) {
            return false;
        }
        Date expiryDate = new Date(millis);

        /* Check if session is expired by comparing
        current date and Session expiry date
        */
        return currentDate.before(expiryDate);
    }

    /**
     * Fetches and returns master details
     *
     * @return master details
     */
    public User getUserDetails() {
        //Check if master is logged in first
        if (!isLoggedIn()) {
            return null;
        }
        User user = new User();
//        idUtilisteur,login,pwd,telephone,name,role,id_bureau,bureau
        user.setIdUtilisteur(mPreferences.getString(KEY_IDUSER, KEY_EMPTY));
        user.setLogin(mPreferences.getString(KEY_LOGIN, KEY_EMPTY));
        user.setPwd(mPreferences.getString(KEY_PWD, KEY_EMPTY));
        user.setTelephone(mPreferences.getString(KEY_TELEPHONE, KEY_EMPTY));
        user.setName(mPreferences.getString(KEY_NAME, KEY_EMPTY));
        user.setRole(mPreferences.getString(KEY_ROLE, KEY_EMPTY));
        user.setId_bureau(mPreferences.getString(KEY_ID_BUREAU, KEY_EMPTY));
        user.setBureau(mPreferences.getString(KEY_BUREAU, KEY_EMPTY));
        user.setCentre(mPreferences.getString(KEY_CENTRE, KEY_EMPTY));
        user.setSessionExpiryDate(new Date(mPreferences.getLong(KEY_EXPIRES, 0)));

        return user;
    }




    /**
     * Logs out master by clearing the session
     */
    public void logoutUser(){
        mEditor.clear();
        mEditor.commit();
    }

}