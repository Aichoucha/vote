<?php

$userArray = array();
$response = array();

include '../db/db_connect.php';
include '../db/fonctions.php';


//Get the input request parameters
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

if( isset($input['centre'])&&isset($input['bureau'])){
    
$centre = $input['centre'];
$bureau = $input['bureau'];

if($centre!=null && $bureau!=null ){

    $nontVotantBureau=getNontVotants($centre,$bureau);
    $nbElecteurBureau=getNbElecteurs($centre,$bureau);

    if($nbElecteurBureau!=null && $nbElecteurBureau!=null){
        //Populate the movie array

        $userArray["centre"] = $centre;
        $userArray["bureau"] = $bureau;
        $userArray["nbElecteur"] = $nbElecteurBureau;
        $userArray["nonVotant"] = $nontVotantBureau;
        $cal = (100*$nontVotantBureau)/$nbElecteurBureau;
        $taux = substr($cal, 0, 5);
//        $taille=strlen($taux);
//        if($taille){
//
//        }

        $userArray["taux"] = $taux." %";

        $response["success"] = 1;
        $response["message"] ="";
        $response["data"] = $userArray;

    }else{
        //When movie is not found
         $response["success"] = 0;
         $response["message"] = "Invalide centre et bureau";
         $response["data"] = $userArray;
    }


}else{
    //Whe some error occurs
    $response["success"] = 0;
    $response["message"] = "Erreur de système";
    $response["data"] = $userArray;

}
}else{
    //When the mandatory parameter movie_id is missing
    $response["status"] = 0;
    $response["message"] = "Parametre manquant";
    $response["data"] = $userArray;
}

//Display JSON response
echo json_encode($response);
?>