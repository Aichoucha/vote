package com.example.hp.svelmobile.helper;
import java.util.Date;

public class Electeur {


    String id_Electeur,numElecteur,numMatricule,nom,prenom,date_naissance,lieu_naissance,pays_nationalite,parcours,statut,genre,etat,id_bureau,bureau,centre;

    Date sessionExpiryDate;

    public String getId_Electeur() {
        return id_Electeur;
    }

    public void setId_Electeur(String id_Electeur) {
        this.id_Electeur = id_Electeur;
    }

    public String getNumElecteur() {
        return numElecteur;
    }

    public void setNumElecteur(String numElecteur) {
        this.numElecteur = numElecteur;
    }

    public String getNumMatricule() {
        return numMatricule;
    }

    public void setNumMatricule(String numMatricule) {
        this.numMatricule = numMatricule;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDate_naissance() {
        return date_naissance;
    }

    public void setDate_naissance(String date_naissance) {
        this.date_naissance = date_naissance;
    }

    public String getLieu_naissance() {
        return lieu_naissance;
    }

    public void setLieu_naissance(String lieu_naissance) {
        this.lieu_naissance = lieu_naissance;
    }

    public String getPays_nationalite() {
        return pays_nationalite;
    }

    public void setPays_nationalite(String pays_nationalite) {
        this.pays_nationalite = pays_nationalite;
    }

    public String getParcours() {
        return parcours;
    }

    public void setParcours(String parcours) {
        this.parcours = parcours;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getId_bureau() {
        return id_bureau;
    }

    public void setId_bureau(String id_bureau) {
        this.id_bureau = id_bureau;
    }

    public Date getSessionExpiryDate() {
        return sessionExpiryDate;
    }

    public void setSessionExpiryDate(Date sessionExpiryDate) {
        this.sessionExpiryDate = sessionExpiryDate;
    }

    public String getBureau() {
        return bureau;
    }

    public void setBureau(String bureau) {
        this.bureau = bureau;
    }

    public String getCentre() {
        return centre;
    }

    public void setCentre(String centre) {
        this.centre = centre;
    }
}
