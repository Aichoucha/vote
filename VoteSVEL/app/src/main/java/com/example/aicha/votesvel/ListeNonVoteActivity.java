package com.example.aicha.votesvel;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filterable;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.aicha.votesvel.helper.ConfigHelper;
import com.example.aicha.votesvel.helper.HttpJsonParser;
import com.example.aicha.votesvel.helper.MySingleton;
import com.example.aicha.votesvel.helper.SessionHandler;
import com.example.aicha.votesvel.helper.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ListeNonVoteActivity extends AppCompatActivity {

    private static final String KEY_DATA = "data";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_SUCCESS = "success";
    private static final String KEY_ID_ELECTEUR = "id_Electeur";
    private static final String KEY_NUMELECTEUR = "numElecteur";
    private static final String KEY_NUMMATRICULE = "numMatricule";
    private static final String KEY_NOM = "nom";
    private static final String KEY_NAME= "name";
    private static final String KEY_PRENOM = "prenom";
    private static final String KEY_DATE_NAISSANCE= "date_naissance";
    private static final String KEY_LIEU_NAISSANCE= "lieu_naissance";
    private static final String KEY_PAYS_NATIONALITE= "pays_nationalite";
    private static final String KEY_PARCOURS= "parcours";
    private static final String KEY_STATUT= "statut";
    private static final String KEY_GENRE= "genre";
    private static final String KEY_ETAT= "etat";
    private static final String KEY_BUERAU= "id_bureau";

    String API_URL;
    String BASE_URL;
    String BASE_URL2;

    private String id_Electeur;
    private String numElecteur;
    private String numMatricule;
    private String id_bureau;
    private String nom;
    private String prenom;
    private String date_naissance;
    private String lieu_naissance;
    private String pays_nationalite;
    private String parcours;
    private String statut;
    private String genre;
    private String etat,name;

    private ArrayList<HashMap<String, String>> approList;
    private ListView approListView;
    private ProgressDialog pDialog;
    private EditText inputSearch;

    private int success;
    private String message,txtNumElecteur;

    private String nbElecteur,nonVotant,taux;

    private TextView textView,textView1,textView2;
//    private String txtView,txtView1,txtView2;

    private SessionHandler session;
    User user;
    Button btnPourcentage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste_non_vote);

        API_URL = ConfigHelper.getConfigValue(this,"api_url");
        BASE_URL = API_URL+"lesNonVotes.php";
        BASE_URL2 = API_URL+"getPourcentageNonVotant.php";

        session = new SessionHandler(getApplicationContext());
        user = session.getUserDetails();

        textView=findViewById(R.id.textView);
        textView1=findViewById(R.id.textView1);
        textView2=findViewById(R.id.textView2);

        setTitle("Les n'ayant pas votés - SVEL | "+user.getName());



        approListView = findViewById(R.id.listeView);

        new ListeNonVoteActivity.FetchVotesAsyncTask().execute();

        load_taux_abs();

    }

    private class FetchVotesAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display progress bar
            pDialog = new ProgressDialog(ListeNonVoteActivity.this);
            pDialog.setMessage("En cours de listing...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpJsonParser httpJsonParser = new HttpJsonParser();
            Map<String, String> httpParams = new HashMap<>();

            httpParams.put("mmesamake", "aicha dembele");
            httpParams.put("id_bureau", user.getId_bureau());

            JSONObject jsonObject = httpJsonParser.makeHttpRequest(
                    BASE_URL, "GET", httpParams);
            try {
                success = jsonObject.getInt(KEY_SUCCESS);
                JSONArray apppros;

                if (success == 1) {
                    approList = new ArrayList<>();

                    apppros = jsonObject.getJSONArray(KEY_DATA);
                    //Iterate through the response and populate appros list
                    for (int i = 0; i < apppros.length(); i++) {
                        JSONObject approvisionnement = apppros.getJSONObject(i);
                        date_naissance = approvisionnement.getString(KEY_DATE_NAISSANCE);
                        nom = approvisionnement.getString(KEY_NOM);
                        prenom = approvisionnement.getString(KEY_PRENOM);
                        name = nom+" "+prenom;
                        Log.d("Electeur ","electeur "+i+" => "+name);
                        numElecteur = approvisionnement.getString(KEY_NUMELECTEUR);
                        numMatricule = approvisionnement.getString(KEY_NUMMATRICULE);
                        lieu_naissance = approvisionnement.getString(KEY_LIEU_NAISSANCE);
                        genre = approvisionnement.getString(KEY_GENRE);
//                        id_bureau = approvisionnement.getString(KEY_BUERAU);


                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(KEY_NUMELECTEUR, numElecteur);
                        map.put(KEY_NAME, name);
                        map.put(KEY_GENRE, genre);
                        map.put(KEY_DATE_NAISSANCE, date_naissance);
                        map.put(KEY_LIEU_NAISSANCE, lieu_naissance);
                        map.put(KEY_NUMMATRICULE, numMatricule);
//                        map.put(KEY_BUERAU, "Bureau № "+id_bureau);

                        approList.add(map);
                    }
                }else {
                    Toast.makeText(getApplicationContext(),
                            jsonObject.getString(KEY_MESSAGE), Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    populateApproList();
                }
            });
        }

    }

    /**
     * Updating parsed JSON data into ListView
     * */
    private void populateApproList() {
        final ListAdapter adapter = new SimpleAdapter(
                ListeNonVoteActivity.this, approList,
                R.layout.list_item_all, new String[] {
                KEY_NAME,
                KEY_NUMELECTEUR
//                KEY_BUERAU,

        }, new int[] {
                R.id.name,
                R.id.numero
//                R.id.detail
        }
        );
        // updating listview
        approListView.setAdapter(adapter);

        approListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Check for network connectivity
                Intent intent=new Intent(getApplicationContext(),DetailElecteurActivity.class);
                String numero=((TextView) view.findViewById(R.id.numero)).getText().toString().trim();
                intent.putExtra("numero",numero);
                startActivity(intent);

            }
        });

        inputSearch = (EditText) findViewById(R.id.inputSearch);
        inputSearch.setSingleLine();

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence cs, int start, int count, int after) {
                ((Filterable) adapter).getFilter().filter(cs);
            }

            @Override
            public void onTextChanged(CharSequence cs, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void load_taux_abs() {
        JSONObject request = new JSONObject();
        try {
            //Populate the request parameters
            request.put("centre", user.getCentre());
            request.put("bureau", user.getBureau());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsArrayRequest = new JsonObjectRequest
                (Request.Method.POST, BASE_URL2, request, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.dismiss();
                        try {
                            //Check if master got logged in successfully

                            if (response.getInt(KEY_SUCCESS) == 1) {
                                JSONObject jsonObject=response.getJSONObject(KEY_DATA);
                                nbElecteur = jsonObject.getString("nbElecteur");
                                nonVotant = jsonObject.getString("nonVotant");
                                taux = jsonObject.getString("taux");

                                Log.d("SESSION","nbElecteur: "+nbElecteur);
                                Log.d("SESSION","nonVotant : "+nonVotant);
                                Log.d("SESSION","taux : "+taux);

                                textView.setText("Nbr électeur : "+nbElecteur);
                                textView1.setText("Non votants : "+nonVotant);
                                textView2.setText("taux d'abstention: "+taux);

                            }else{

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();

                        //Display error message whenever an error occurs
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsArrayRequest);
    }

}
