<?php
/**
 * Created by PhpStorm.
 * User: AICHA
 * Date: 01/01/2019
 * Time: 03:30
 */

$BASE_URL="http://10.0.2.2/Web_Service_Vote/";
//$BASE_URL="http://www.maliafrique.ml/Web_Service_Vote/";

// EN LOCAL
define('DB_USER', "root"); // db user
define('DB_PASSWORD', ""); // db password (mention your db password here)
define('DB_DATABASE', "voteelectronique"); // database name
define('DB_SERVER', "localhost"); // db local

// SUR LE SERVEUR
//define('DB_USER', "maliafrique_test"); // db user
//define('DB_PASSWORD', "aichadembele"); // db password (mention your db password here)
//define('DB_DATABASE', "maliafrique_test"); // database name
//define('DB_SERVER', "localhost"); // db local

$con = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);

// Check connection
if(mysqli_connect_errno()){
	echo "Impossible de seconnecter à la base : " . mysqli_connect_error();
}

?>
