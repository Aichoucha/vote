package com.example.aicha.votesvel;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aicha.votesvel.helper.ConfigHelper;
import com.example.aicha.votesvel.helper.HttpJsonParser;
import com.example.aicha.votesvel.helper.SessionHandler;
import com.example.aicha.votesvel.helper.StringWithTag;
import com.example.aicha.votesvel.helper.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CentreBureauActivity extends AppCompatActivity {

    private static final String KEY_DATA = "data";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_SUCCESS = "success";
    private static final String KEY_BUERAU= "id_bureau";

    private static final String KEY_EMPTY = "";

    private static final String KEY_NAME = "name";
    private static final String KEY_TELEPHONE= "telephone";
    private static final String KEY_CENTRE= "centre";
    private static final String KEY_ID_BUREAU= "id_bureau";
    private static final String KEY_NOM_BUREAU= "nom_bureau";
    private static final String KEY_IDUSER= "idUser";
    private static final String KEY_REFERENCE= "reference";

    private String id_bureau,nom_bureau;

    String API_URL;
    String BASE_URL;

    private Button btnCentre,btnBureau,btnResultat;
    private TextView txtName;
    private Spinner spinner;
    private String centre,bureau_bureau;

    private SessionHandler session;
    User user;
    String role;
    String loadsearchkeyID;

    private Intent intent;

    Map<String, String> spinnerValueMap = new HashMap<String, String>();
    List<StringWithTag> itemList = new ArrayList<StringWithTag>();
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_centre_bureau);
        setTitle("Centre ");

        API_URL = ConfigHelper.getConfigValue(this,"api_url");
        BASE_URL = API_URL+"getBureaux.php";

        session = new SessionHandler(getApplicationContext());
        user=session.getUserDetails();
        role=user.getRole();
        centre=user.getCentre();

        setTitle("Chef du "+user.getCentre());

        btnCentre=findViewById(R.id.btnCentre);
        btnCentre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(CentreBureauActivity.this,ResultatActivity.class);
                intent.putExtra("type","parcentre");
                startActivity(intent);


            }
        });
        btnBureau=findViewById(R.id.btnBureau);
        btnBureau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        intent=new Intent(CentreBureauActivity.this,ListeElecteursActivity.class);
        if(id_bureau!=null && bureau_bureau!=null){
            intent.putExtra("id_bureau",id_bureau);
            intent.putExtra("centre",user.getCentre());
            intent.putExtra("bureau",bureau_bureau);
            startActivity(intent);
        }else{
            Toast.makeText(getApplicationContext(),"Erreur de choix du bureau", Toast.LENGTH_LONG).show();
        }

            }
        });
        btnResultat=findViewById(R.id.btnResultat);
        btnResultat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(CentreBureauActivity.this,ProvisoireActivity.class);
                if(id_bureau!=null && bureau_bureau!=null){
//                    intent.putExtra("id_bureau",id_bureau);
                    intent.putExtra("centre",user.getCentre());
                    intent.putExtra("bureau",bureau_bureau);
                    startActivity(intent);
                }else{
                    Toast.makeText(getApplicationContext(),"Erreur de choix du bureau", Toast.LENGTH_LONG).show();
                }
            }
        });
        spinner = (Spinner) findViewById(R.id.spinner);

        txtName=findViewById(R.id.txtName);
        txtName.setText("Bienvenu "+user.getName());

        new FetchBureauAsyncTask().execute();

    }


    /**
     * Fetches the list of appros from the server
     */
    private class FetchBureauAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display progress bar
            pDialog = new ProgressDialog(CentreBureauActivity.this);
            pDialog.setMessage("En cours de listing...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpJsonParser httpJsonParser = new HttpJsonParser();
            Map<String, String> httpParams = new HashMap<>();
            centre=user.getCentre();
            httpParams.put(KEY_CENTRE, centre );

            JSONObject jsonObject = httpJsonParser.makeHttpRequest(BASE_URL , "GET", httpParams);
            try {
                int success = jsonObject.getInt(KEY_SUCCESS);
                JSONArray apppros;

                if (success == 1) {
                    apppros = jsonObject.getJSONArray(KEY_DATA);
                    for (int i = 0; i < apppros.length(); i++) {
                        JSONObject approvisionnement = apppros.getJSONObject(i);
                        id_bureau = approvisionnement.getString(KEY_ID_BUREAU);
                        nom_bureau = approvisionnement.getString(KEY_NOM_BUREAU);
                        spinnerValueMap.put(id_bureau,nom_bureau);
                    }

                    for (Map.Entry<String, String> entry : spinnerValueMap.entrySet()) {
                        String key = entry.getKey();
                        String value = entry.getValue();

                        /* Build the StringWithTag List using these keys and values. */
                        itemList.add(new StringWithTag(key, value));
                    }

                }else {
                    Toast.makeText(getApplicationContext(),
                            jsonObject.getString(KEY_MESSAGE), Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    populateApproList();
                }
            });
        }

    }

    /**
     * Updating parsed JSON data into ListView
     * */
    private void populateApproList() {

        ArrayAdapter<StringWithTag> spinnerAdapter = new ArrayAdapter<StringWithTag>(getApplicationContext(), android.R.layout.simple_spinner_item, itemList);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StringWithTag swt = (StringWithTag) parent.getItemAtPosition(position);
                loadsearchkeyID = (String) swt.tag;
                id_bureau=(String) swt.tag;
                bureau_bureau=parent.getItemAtPosition(position).toString();

//                id_bureau=parent.getItemAtPosition(position).toString();
//                txtName.setText(parent.getItemAtPosition(position).toString());
//                txtTelephone.setText(loadsearchkeyID);
//                Toast.makeText(getApplicationContext(), "NOM : "+parent.getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show();
//                Toast.makeText(getApplicationContext(), "ID : "+loadsearchkeyID, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }


}
