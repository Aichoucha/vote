<?php
/**
 * Created by PhpStorm.
 * User: TIEZ
 * Date: 02/12/2018
 * Time: 03:30
 */

  include '../db/db_connect.php';
    include '../db/fonctions.php';
//Query to select movie id and movie name
$result = array();
$userArray = array();
$response = array();

//Get the input request parameters

//Check for mandatory parameter
if(isset($_GET['mmesamake'])&& isset($_GET['id_bureau']) ){

    $id_bureau=$_GET['id_bureau'];

$query = "SELECT * FROM electeur WHERE etat='A VOTE' AND id_bureau='$id_bureau' ORDER BY nom and prenom";

//Prepare the query
if($stmt = $con->prepare($query)){
	$stmt->execute();
	//Bind the fetched data to $movieId and $movieName
	$stmt->bind_result($id_Electeur,$numElecteur,$numMatricule,$nom,$prenom,$date_naissance,$lieu_naissance,$pays_nationalite,$parcours,$statut,$genre,$etat,$id_bureau);
	//Fetch 1 row at a time					
	while($stmt->fetch()){
		//Populate the movie array
		                
                $userArray["id_Electeur"] = $id_Electeur;
                $userArray["numElecteur"] = $numElecteur;
                $userArray["numMatricule"] = $numMatricule;
                $userArray["nom"] = $nom;
                $userArray["prenom"] = $prenom;
                $date=format_date($date_naissance);
                $userArray["date_naissance"] = $date;
                $userArray["lieu_naissance"] = $lieu_naissance;
                $userArray["pays_nationalite"] = $pays_nationalite;
                $userArray["parcours"] = $parcours;
                $userArray["statut"] = $statut;
                $userArray["genre"] = $genre;
                $userArray["etat"] = $etat;
                $userArray["id_bureau"] = $id_bureau;
                
		$result[]=$userArray;
		
	}

   

	
	$response["success"] = 1;
	$response["data"] = $result;
        $response["message"] = "En cours de listing...";
        
        $stmt->close();
	

}else{
	//Some error while fetching data
	$response["success"] = 0;
        $response["data"] = $result;
	$response["message"] = "Erreur du serveur";
	
}

}else{
	//When the mandatory parameter movie_id is missing
	$response["status"] = 0;
        $response["data"] = $result;
	$response["message"] = "Parametre manquant";
        
}
//Display JSON response
echo json_encode($response);

?>