package com.example.aicha.votesvel;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.aicha.votesvel.helper.SessionHandler;
import com.example.aicha.votesvel.helper.User;

public class ConnaitreBureauActivity extends AppCompatActivity {

    private TextView txtCentre,txtResut;
    private Button btnQR,btnNUM;
    private SessionHandler session;
    User user;
    String role;

    private Intent intent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connaitre_bureau);

        session = new SessionHandler(getApplicationContext());
        user=session.getUserDetails();
        role=user.getRole();

        txtCentre=findViewById(R.id.txtCentre);
        txtCentre.setText(user.getName());

        btnQR=findViewById(R.id.btnQR);
        btnQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(ConnaitreBureauActivity.this,Verification2Activity.class);
                startActivity(intent);
            }
        });
        btnNUM=findViewById(R.id.btnNUM);
        btnNUM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent=new Intent(ConnaitreBureauActivity.this,VerificationActivity.class);
                startActivity(intent);
            }
        });


    }
}
