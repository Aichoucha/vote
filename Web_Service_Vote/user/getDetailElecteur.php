<?php

$userArray = array();
$response = array();



//Get the input request parameters
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

if( isset($input['numElecteur']) ){
	
include '../db/db_connect.php';
include '../db/fonctions.php';

    $numElecteur = $input['numElecteur'];


//$query = "SELECT * FROM electeur WHERE etat=?";
$query = "SELECT e.id_Electeur as id_Electeur ,e.numElecteur as numElecteur,e.numMatricule as numMatricule ,e.nom as nom,e.prenom as prenom ,e.date_naissance as date_naissance,e.lieu_naissance as lieu_naissance,e.pays_nationalite as pays_nationalite,e.parcours as parcours,e.statut as statut,e.genre as genre,e.etat as etat,e.id_bureau as id_bureau,b.nom_bureau as bureau,c.nom_centre as centre
FROM electeur e,bureau b, centre c
WHERE e.id_bureau=b.id AND b.centre_id=c.id AND numElecteur=?";
if($stmt = $con->prepare($query)){
    //Bind movie_id parameter to the query
    $stmt->bind_param("s",$numElecteur);
    $stmt->execute();
    //Bind fetched result to variables id,reference,name,telephone,email,password,image,etat,role,date_adhesion
    $stmt->bind_result($id_Electeur,$numElecteur,$numMatricule,$nom,$prenom,$date_naissance,$lieu_naissance,$pays_nationalite,$parcours,$statut,$genre,$etat,$id_bureau,$bureau,$centre);
    //Check for results

    if($stmt->fetch()){
        //Populate the movie array
        $userArray["id_Electeur"] = $id_Electeur;
        $userArray["numElecteur"] = $numElecteur;
        $userArray["numMatricule"] = $numMatricule;
        $userArray["nom"] = $nom;
        $userArray["prenom"] = $prenom;
        $userArray["date_naissance"] = format_date($date_naissance);
        $userArray["lieu_naissance"] = $lieu_naissance;
        $userArray["pays_nationalite"] = $pays_nationalite;
        $userArray["parcours"] = $parcours;
        $userArray["statut"] = $statut;
        $userArray["genre"] = $genre;
        $userArray["etat"] = $etat;
        $userArray["id_bureau"] = $id_bureau;
        $userArray["bureau"] = $bureau;
        $userArray["centre"] = $centre;

        $response["success"] = 1;
        $response["message"] ="Un électeur Trouvé";
        $response["data"] = $userArray;

    }else{
        //When movie is not found
         $response["success"] = 0;
         $response["message"] = "Aucun électeur en cours !";
         $response["data"] = $userArray;
    }
    $stmt->close();


}else{
    //Whe some error occurs
    $response["success"] = 0;
    $response["message"] = "Erreur de système";
    $response["data"] = $userArray;

}

}else{
    //When the mandatory parameter movie_id is missing
    $response["status"] = 0;
    $response["message"] = "Parametre manquant";
    $response["data"] = $userArray;
}

//Display JSON response
echo json_encode($response);
?>