package com.example.hp.svelmobile.helper;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.Date;

public class SessionHandler {

    //    idUtilisteur,login,pwd,telephone,name,role,id_bureau,bureau
    private static final String KEY_IDUSER = "idUtilisteur";
    private static final String KEY_LOGIN = "login";
    private static final String KEY_PWD = "pwd";
    private static final String KEY_TELEPHONE = "telephone";
    private static final String KEY_NAME = "name";
    private static final String KEY_ROLE = "role";

// id_Electeur,numElecteur,numMatricule,nom,prenom,date_naissance,lieu_naissance,pays_nationalite,parcours,statut,genre,etat,id_bureau,bureau
    private static final String PREF_NAME = "UserSession";
    private static final String KEY_IDELECTEUR = "id_Electeur";
    private static final String KEY_NUM_EELECTEUR = "numElecteur";
    private static final String KEY_MATRICULE = "numMatricule";
    private static final String KEY_NON = "nom";
    private static final String KEY_PRENOM = "prenom";
    private static final String KEY_DATE_NAISSANCE = "date_naissance";
    private static final String KEY_LIEU = "lieu_naissance";
    private static final String KEY_PAYS = "pays_nationalite";
    private static final String KEY_PARCOURS= "parcours";
    private static final String KEY_STATUT = "statut";
    private static final String KEY_GENRE = "genre";
    private static final String KEY_ETAT = "etat";
    private static final String KEY_ID_BUREAU = "id_bureau";
    private static final String KEY_BUREAU = "bureau";
    private static final String KEY_CENTRE = "centre";

    private static final String KEY_EMPTY = "";

    private static final String KEY_EXPIRES = "expires";

    Date sessionExpiryDate;


    private Context mContext;
    private SharedPreferences.Editor mEditor;
    private SharedPreferences mPreferences;


    public SessionHandler(Context mContext) {
        this.mContext = mContext;
        mPreferences = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        this.mEditor = mPreferences.edit();
    }

    public void loginUser(String idUtilisteur,String login,String pwd,String telephone,String name,String role,String id_bureau,String bureau,String centre) {
        mEditor.putString(KEY_IDUSER, idUtilisteur);
        mEditor.putString(KEY_LOGIN, login);
        mEditor.putString(KEY_PWD, pwd);
        mEditor.putString(KEY_TELEPHONE, telephone);
        mEditor.putString(KEY_NAME, name);
        mEditor.putString(KEY_ROLE, role);
        mEditor.putString(KEY_ID_BUREAU, id_bureau);
        mEditor.putString(KEY_BUREAU, bureau);
        mEditor.putString(KEY_CENTRE, centre);
        Date date = new Date();

        //Set master session for next 1 days
//        long millis = date.getTime() + (10 * 60 * 60 * 1000);
        long millis = date.getTime() + (10 * 60 * 60 * 1000);
        mEditor.putLong(KEY_EXPIRES, millis);
        mEditor.commit();
    }
    /**
    * Logs in the master by saving master details and setting session
     *
     *
     */
    public void voteElecteur(
            String id_Electeur,
            String numElecteur,
            String numMatricule,
            String nom,
            String prenom,
            String date_naissance,
            String lieu_naissance,
            String pays_nationalite,
            String parcours,
            String statut,
            String genre,
            String etat,
            String id_bureau,
            String bureau,
            String centre
    ) {
        mEditor.putString(KEY_IDELECTEUR, id_Electeur);
        mEditor.putString(KEY_NUM_EELECTEUR, numElecteur);
        mEditor.putString(KEY_MATRICULE, numMatricule);
        mEditor.putString(KEY_NON, nom);
        mEditor.putString(KEY_PRENOM, prenom);
        mEditor.putString(KEY_DATE_NAISSANCE, date_naissance);
        mEditor.putString(KEY_LIEU, lieu_naissance);
        mEditor.putString(KEY_PAYS, pays_nationalite);
        mEditor.putString(KEY_PARCOURS, parcours);
        mEditor.putString(KEY_STATUT, statut);
        mEditor.putString(KEY_GENRE, genre);
        mEditor.putString(KEY_ETAT, etat);
        mEditor.putString(KEY_ID_BUREAU, id_bureau);
        mEditor.putString(KEY_BUREAU, bureau);
        mEditor.putString(KEY_CENTRE, centre);
        Date date = new Date();

        //Set master session for next 7 days
//        long millis = date.getTime() + (7 * 24 * 60 * 60 * 1000);
        long millis = date.getTime() + (5 * 60 * 1000);  // 5 minutes
        mEditor.putLong(KEY_EXPIRES, millis);
        mEditor.commit();
    }



    /**
     * Checks whether master is logged in
     *
     * @return
     */
    public boolean isLoggedIn() {
        Date currentDate = new Date();

        long millis = mPreferences.getLong(KEY_EXPIRES, 0);

        /* If shared preferences does not have a value
         then master is not logged in
         */
        if (millis == 0) {
            return false;
        }
        Date expiryDate = new Date(millis);

        /* Check if session is expired by comparing
        current date and Session expiry date
        */
        return currentDate.before(expiryDate);
    }

    /**
     * Fetches and returns master details
     *
     * @return master details
     */
    public User getUserDetails() {
        //Check if master is logged in first
        if (!isLoggedIn()) {
            return null;
        }
        User user = new User();
//        idUtilisteur,login,pwd,telephone,name,role,id_bureau,bureau
        user.setIdUtilisteur(mPreferences.getString(KEY_IDUSER, KEY_EMPTY));
        user.setLogin(mPreferences.getString(KEY_LOGIN, KEY_EMPTY));
        user.setPwd(mPreferences.getString(KEY_PWD, KEY_EMPTY));
        user.setTelephone(mPreferences.getString(KEY_TELEPHONE, KEY_EMPTY));
        user.setName(mPreferences.getString(KEY_NAME, KEY_EMPTY));
        user.setRole(mPreferences.getString(KEY_ROLE, KEY_EMPTY));
        user.setId_bureau(mPreferences.getString(KEY_ID_BUREAU, KEY_EMPTY));
        user.setBureau(mPreferences.getString(KEY_BUREAU, KEY_EMPTY));
        user.setCentre(mPreferences.getString(KEY_CENTRE, KEY_EMPTY));
        user.setSessionExpiryDate(new Date(mPreferences.getLong(KEY_EXPIRES, 0)));

        return user;
    }

    /**
     * Fetches and returns master details
     *
     * @return master details
     */
    public Electeur getElecteurDetails() {
        //Check if master is logged in first
        if (!isLoggedIn()) {
            return null;
        }
        Electeur e = new Electeur();
       // id_Electeur,numElecteur,numMatricule,nom,prenom,date_naissance,lieu_naissance,pays_nationalite,parcours,statut,genre,etat,id_bureau,bureau
        e.setId_Electeur(mPreferences.getString(KEY_IDELECTEUR, KEY_EMPTY));
        e.setNumElecteur(mPreferences.getString(KEY_NUM_EELECTEUR, KEY_EMPTY));
        e.setNumMatricule(mPreferences.getString(KEY_MATRICULE, KEY_EMPTY));
        e.setNom(mPreferences.getString(KEY_NON, KEY_EMPTY));
        e.setPrenom(mPreferences.getString(KEY_PRENOM, KEY_EMPTY));
        e.setDate_naissance(mPreferences.getString(KEY_DATE_NAISSANCE, KEY_EMPTY));
        e.setLieu_naissance(mPreferences.getString(KEY_LIEU, KEY_EMPTY));
        e.setPays_nationalite(mPreferences.getString(KEY_PAYS,KEY_EMPTY));

        e.setParcours(mPreferences.getString(KEY_PARCOURS,KEY_EMPTY));
        e.setStatut(mPreferences.getString(KEY_STATUT,KEY_EMPTY));
        e.setGenre(mPreferences.getString(KEY_GENRE,KEY_EMPTY));
        e.setEtat(mPreferences.getString(KEY_ETAT,KEY_EMPTY));
        e.setId_bureau(mPreferences.getString(KEY_ID_BUREAU,KEY_EMPTY));
        e.setBureau(mPreferences.getString(KEY_BUREAU,KEY_EMPTY));
        e.setCentre(mPreferences.getString(KEY_CENTRE,KEY_EMPTY));

        e.setSessionExpiryDate(new Date(mPreferences.getLong(KEY_EXPIRES, 0)));

        return e;
    }




    /**
     * Logs out master by clearing the session
     */
    public void logoutUser(){
        mEditor.clear();
        mEditor.commit();
    }

}