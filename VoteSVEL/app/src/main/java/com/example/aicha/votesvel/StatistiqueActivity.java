package com.example.aicha.votesvel;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.example.aicha.votesvel.helper.ConfigHelper;
import com.example.aicha.votesvel.helper.HttpJsonParser;
import com.example.aicha.votesvel.helper.RSAUtil;
import com.example.aicha.votesvel.helper.SessionHandler;
import com.example.aicha.votesvel.helper.User;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.aicha.votesvel.helper.Hashmap.sortByValue;
import static com.example.aicha.votesvel.helper.Hashmap.sortByValueArrayList;

public class StatistiqueActivity extends AppCompatActivity {

    private static final String KEY_SUCCESS = "success";
    private static final String KEY_DATA = "data";
    private static final String KEY_MESSAGE = "message";

    private static final String KEY_PARTI= "parti";
    private static final String KEY_ID_CANDIDAT= "id_Candidat";
    private static final String KEY_NOM= "nom";
    private static final String KEY_PRENOM= "prenom";
    private static final String KEY_GENRE= "genre";
    private static final String KEY_PHOTO= "photo";



    private static final String KEY_EMPTY = "";
    //    private static final String KEY_BUREAU= "bureau";
    private static final String KEY_CENTRE= "centre";

    String BASE_URL,API_URL;
    String URL_CANDIDAT;

    private Button button;

    private String elect,aicha,voix,pa,parti,photo,nom,prenom,bureau,centre,electeur,genre,NumCandidat;

    HashMap<String, String> mapTemp1;
    HashMap<String, String> mapVote;
    HashMap<String, String> mapCandidat;
    HashMap<String, String> mapTemp,mapTemp2,mapTemp3,mapTemp4;

    private ArrayList<HashMap<String, String>> VoteList;
    private ArrayList<HashMap<String, String>> candidatList;

    HashMap<String, String> mapVoix;
    HashMap<String, String> mapVoixOrder;
    HashMap<String, String> mapName;
    HashMap<String, String> mapPhoto;

    private ArrayList<HashMap<String, String>> mapNameList;
    private ArrayList<HashMap<String, String>> mapPhotoList;
    private ArrayList<HashMap<String, String>> mapVoixList;
    private HashMap<String, String> Order;
    private HashMap<String, String> mapVoixListOrder;
    private ArrayList<HashMap<String, String>> tempVoteList;
    private ArrayList<HashMap<String, String>> mapVoteList;
    private ArrayList<HashMap<String, String>> mapVoixOrderList;
    private String nb_electeur,nb_votant,role;
    private int success;
    private String message;
    private ProgressDialog pDialog;

    private SessionHandler session;
    User user;

    String type;

    PieChart pieChart;
    BarChart chart;
    ArrayList<Integer> nbVote;
    ArrayList<String> aParti;
    int nbElecteurVotant;
    Context ctx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistique);
        ctx=this;
        API_URL = ConfigHelper.getConfigValue(this,"api_url");
        URL_CANDIDAT = API_URL+"getAllCandidats.php";



        nbElecteurVotant=0;

        chart = (BarChart) findViewById(R.id.barchart);

        nbVote=new ArrayList<>();
        aParti=new ArrayList<>();

        session = new SessionHandler(getApplicationContext());
        user = session.getUserDetails();

        session = new SessionHandler(getApplicationContext());
        user = session.getUserDetails();
        final Intent intent=getIntent();
        final String id_centre=intent.getStringExtra("id_centre");
        final String non_centre=intent.getStringExtra("nom_centre");
        type=intent.getStringExtra("type");

        if(id_centre!=null & non_centre!=null){
            centre=non_centre;
        }else{
            centre=user.getCentre();
        }

        if(type.equals("global")){
            BASE_URL = API_URL+"resultatprosoireglobal.php";
            setTitle("Resultat provisoire global ");
            aicha= "Stistiques provisoires globaux";
        }else if(type.equals("parcentre")){
            setTitle("Statistiques provisoires du - "+centre);
            aicha= "Stistiques provisoires du "+centre;
            BASE_URL = API_URL+"resultatprosoirecentre.php";
        }

        Log.d("LOG","\n\nid_centre : "+id_centre+"\nnom_centre : "+centre+"\ntype de resultat : "+type);


        new StatistiqueActivity.FetchAllCandidatsAsyncTask().execute();



    }


    /**
     * Fetches the list of appros from the server
     */
    private class FetchResultatAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display progress bar
            pDialog = new ProgressDialog(StatistiqueActivity.this);
            pDialog.setMessage("Calcul en cours...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected String doInBackground(String... params) {
            HttpJsonParser httpJsonParser = new HttpJsonParser();
            Map<String, String> httpParams = new HashMap<>();

            httpParams.put(KEY_CENTRE, centre);
            Log.d("KEY_CENTRE","centre : "+centre);
//            httpParams.put(KEY_BUREAU, bureau);


            JSONObject jsonObject = httpJsonParser.makeHttpRequest(
                    BASE_URL, "GET", httpParams);
            try {
                int success = jsonObject.getInt(KEY_SUCCESS);
                JSONArray apppros;

                if (success == 1) {
                    VoteList = new ArrayList<>();

                    apppros = jsonObject.getJSONArray(KEY_DATA);

                    for (int i = 0; i < apppros.length(); i++) {

                        nbElecteurVotant++;

                        JSONObject p = apppros.getJSONObject(i);

                        pa = p.getString(KEY_PARTI);
                        parti=RSAUtil.decrypt(ctx,pa);
                        voix = "0";
                        centre = p.getString(KEY_CENTRE);

                        Log.d("vote","parti : "+i+" "+pa+"  "+parti);

                        mapVote = new HashMap<String, String>();
                        mapVote.put(parti, voix);

                        VoteList.add(mapVote);
                    }

                    Log.d("KEY_CENTRE","centre : "+centre);
                }else {
                    Toast.makeText(getApplicationContext(),
                            jsonObject.getString(KEY_MESSAGE), Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    populateApproList();
                }
            });
        }

    }

    private class FetchAllCandidatsAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display progress bar
            pDialog = new ProgressDialog(StatistiqueActivity.this);
            pDialog.setMessage("Affichage en cours...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpJsonParser httpJsonParser = new HttpJsonParser();

            Map<String, String> httpParams = new HashMap<>();
            httpParams.put("getAllCandidats", "getAllCandidats");


            JSONObject jsonObject = httpJsonParser.makeHttpRequest(
                    URL_CANDIDAT, "GET", httpParams);
            try {
                int success = jsonObject.getInt(KEY_SUCCESS);
                JSONArray apppros;

                if (success == 1) {

                    candidatList = new ArrayList<>();

                    mapNameList = new ArrayList<>();
                    mapVoixList = new ArrayList<>();
                    mapPhotoList = new ArrayList<>();

                    apppros = jsonObject.getJSONArray(KEY_DATA);



                    byte[] Photodata;
                    //Iterate through the response and populate appros list
                    for (int i = 0; i < apppros.length(); i++) {
                        JSONObject approvisionnement = apppros.getJSONObject(i);

                        NumCandidat = approvisionnement.getString(KEY_ID_CANDIDAT);
                        nom = approvisionnement.getString(KEY_NOM);
                        prenom = approvisionnement.getString(KEY_PRENOM);
                        genre = approvisionnement.getString(KEY_GENRE);
                        photo = approvisionnement.getString(KEY_PHOTO);
                        parti = approvisionnement.getString(KEY_PARTI);

                        voix="0";

                        mapCandidat = new HashMap<String, String>();

                        mapVoix = new HashMap<String, String>();
                        mapName = new HashMap<String, String>();
                        mapPhoto = new HashMap<String, String>();

                        mapCandidat.put(KEY_PHOTO, photo);
                        mapCandidat.put(parti, voix);
                        mapCandidat.put(KEY_PRENOM, nom+" "+prenom);

                        candidatList.add(mapCandidat);



                        mapPhoto.put(parti, photo);
                        mapVoix.put(parti, voix);
                        mapName.put(parti, nom+" "+prenom);

                        mapNameList.add(mapName);

                        mapPhotoList.add(mapPhoto);
                        mapVoixList.add(mapVoix);
                    }

                }else {
                    Toast.makeText(getApplicationContext(),
                            jsonObject.getString(KEY_MESSAGE), Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {

                    new FetchResultatAsyncTask().execute();

                }
            });
        }

    }


    /**
     * Updating parsed JSON data into ListView
     * */
    private void populateApproList() {

        Log.d("aicha","VoteList");
        if(VoteList.size()>0){
            for(int j=0;j<VoteList.size();j++){
                mapTemp3=VoteList.get(j);
                for(int k=0;k<VoteList.size();k++){
                    mapTemp4=VoteList.get(k);
                    for (Map.Entry<String, String> candidat : mapTemp3.entrySet()){
                        for (Map.Entry<String, String> vote : mapTemp4.entrySet()){
                            if(candidat.getKey().equals(vote.getKey())){
                                int nb=Integer.parseInt(candidat.getValue())+1;
                                candidat.setValue(String.valueOf(nb));
                            }
                        }
                    }
                }
            }
        }


        for(int i=0;i<mapVoixList.size();i++){
            mapTemp1=mapVoixList.get(i);
            if(VoteList.size()>0){
                for(int j=0;j<VoteList.size();j++){
                    mapTemp2=VoteList.get(j);
                    for (Map.Entry<String, String> candidat : mapTemp1.entrySet()){
                        for (Map.Entry<String, String> vote : mapTemp2.entrySet()){
                            if(candidat.getKey().equals(vote.getKey())){
                                candidat.setValue(vote.getValue());
                            }

                        }
                    }
                }
            }
        }


        mapVoixListOrder=sortByValueArrayList(mapVoixList);
        Log.d("order","avant ordonner");
        HashMap<String,String> test=mapVoixListOrder;
        for(Map.Entry<String,String> maptest:test.entrySet()){
            Log.d("order","key : "+maptest.getKey()+"  value : "+maptest.getValue());

        }

        mapVoixListOrder=sortByValue(mapVoixListOrder);
        Log.d("order","apres ordonner");
        HashMap<String,String> oderMapVoix=mapVoixListOrder;
        int fst=0;
        for(Map.Entry<String,String> voix:oderMapVoix.entrySet()){
            Log.d("order","HashMap " +fst+ " key : "+voix.getKey()+"  value : "+voix.getValue());
            nbVote.add(Integer.parseInt(voix.getValue()));
            aParti.add(voix.getKey());
        }




        BarData data = new BarData(aParti, getDataSet(nbVote));
        chart.setData(data);
        chart.setDescription("");
        chart.animateXY(2000, 2000);
        chart.invalidate();
    }





    private ArrayList<BarDataSet> getDataSet( ArrayList<Integer> val) {
        ArrayList<BarDataSet> dataSets = null;

        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        for(int k=0;k<val.size();k++){
            BarEntry v1e1 = new BarEntry(val.get(k), k);
            valueSet1.add(v1e1);
        }

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "Légende");
        barDataSet1.setColors(ColorTemplate.COLORFUL_COLORS);

        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);
        return dataSets;
    }

//    private ArrayList<String> getXAxisValues(ArrayList<String> xAxis) {
////        ArrayList<String> xAxis = new ArrayList<>();
////        for(int a=0;a<xAxis.size();a++){
////            xAxis.add(xAxis.get(a));
////        }
////        xAxis.add("JAN");
////        xAxis.add("FEB");
////        xAxis.add("MAR");
////        xAxis.add("APR");
////        xAxis.add("MAY");
////        xAxis.add("JUN");
//        return xAxis;
//    }




}
