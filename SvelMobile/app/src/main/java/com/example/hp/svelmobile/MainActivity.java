package com.example.hp.svelmobile;

import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.hp.svelmobile.helper.BackgroundService;
import com.example.hp.svelmobile.helper.ConfigHelper;
import com.example.hp.svelmobile.helper.Electeur;
import com.example.hp.svelmobile.helper.HttpJsonParser;
import com.example.hp.svelmobile.helper.MySingleton;
import com.example.hp.svelmobile.helper.SessionHandler;
import com.example.hp.svelmobile.helper.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.example.hp.svelmobile.helper.User.mediaPlayer;

public class MainActivity extends AppCompatActivity {

    private static final String KEY_DATA = "data";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_SUCCESS = "success";
    private static final String KEY_ID_ELECTEUR = "id_Electeur";
    private static final String KEY_NUMELECTEUR = "numElecteur";
    private static final String KEY_NUMMATRICULE = "numMatricule";
    private static final String KEY_NOM = "nom";
    private static final String KEY_PRENOM = "prenom";
    private static final String KEY_DATE_NAISSANCE= "date_naissance";
    private static final String KEY_LIEU_NAISSANCE= "lieu_naissance";
    private static final String KEY_PAYS_NATIONALITE= "pays_nationalite";
    private static final String KEY_PARCOURS= "parcours";
    private static final String KEY_STATUT= "statut";
    private static final String KEY_GENRE= "genre";
    private static final String KEY_ETAT= "etat";
    private static final String KEY_ID_BUREAU= "id_bureau";
    private static final String KEY_BUREAU = "bureau";
    private static final String KEY_CENTRE = "centre";

    String API_URL;
    String BASE_URL;

    private String id_Electeur,numElecteur,numMatricule,nom,prenom,date_naissance,lieu_naissance,pays_nationalite,parcours,statut,genre,etat,id_bureau,bureau,centre;
    private TextView txtElecteur,txtInfo;

    private int success;
    private String message;
    private ProgressDialog pDialog;

    private Button btnFrancais,btnBambara;

    SessionHandler session;
    Electeur electeur;
    User user;

    TextView txtB;

    Intent smsServiceIntent;
    static BackgroundService backgroundService;
    Intent mServiceIntent;
    Context ctx;
    public Context getCtx() {
        return ctx;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ctx = this;
        API_URL = ConfigHelper.getConfigValue(this,"api_url");
        BASE_URL = API_URL+"getElecteurVeutVoter.php";

        setTitle("SVEL - MOBILE");

        txtB=findViewById(R.id.txtB);

        session = new SessionHandler(getApplicationContext());
        user=session.getUserDetails();
        txtB.setText(user.getCentre()+" : "+user.getBureau());


        btnFrancais=findViewById(R.id.btnFrançais);
        btnBambara=findViewById(R.id.btnBambara);

        txtElecteur=findViewById(R.id.txtElecteur);
        txtInfo=findViewById(R.id.txtInfo);


        defaultLangue();
        loadElecteur(); // load electeur info

        btnBambara.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bambaraLangue();
                Toast.makeText(getApplicationContext(),"Bambara choisi avec succès", Toast.LENGTH_LONG).show();
                Intent i = new Intent(getApplicationContext(), VoteActivity.class);
                i.putExtra("langue","bambara");
                i.putExtra("idElecteur",id_Electeur);
                i.putExtra("numElecteur",numElecteur);
                i.putExtra("numMatricule",numMatricule);
                i.putExtra("nom",nom);
                i.putExtra("prenom",prenom);
                i.putExtra("date_naissance",date_naissance);
                i.putExtra("lieu_naissance",lieu_naissance);
                i.putExtra("pays_nationalite",pays_nationalite);
                i.putExtra("parcours",parcours);
                i.putExtra("statut",statut);
                i.putExtra("genre",genre);
                i.putExtra("etat",etat);
                i.putExtra("id_bureau",id_bureau);
                i.putExtra("bureau",bureau);
                i.putExtra("centre",centre);
                startActivity(i);
                finish();
            }
        });
        btnFrancais.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                francaisLangue();
                Toast.makeText(getApplicationContext(),"Francais choisi avec succès", Toast.LENGTH_LONG).show();
                Intent i = new Intent(getApplicationContext(), VoteActivity.class);
                i.putExtra("langue","francais");
                i.putExtra("idElecteur",id_Electeur);
                i.putExtra("numElecteur",numElecteur);
                i.putExtra("numMatricule",numMatricule);
                i.putExtra("nom",nom);
                i.putExtra("prenom",prenom);
                i.putExtra("date_naissance",date_naissance);
                i.putExtra("lieu_naissance",lieu_naissance);
                i.putExtra("pays_nationalite",pays_nationalite);
                i.putExtra("parcours",parcours);
                i.putExtra("statut",statut);
                i.putExtra("genre",genre);
                i.putExtra("etat",etat);
                i.putExtra("id_bureau",id_bureau);
                i.putExtra("bureau",bureau);
                i.putExtra("centre",centre);
                startActivity(i);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void defaultLangue(){

            mediaPlayer=MediaPlayer.create(this,R.raw.bienvenuvocal);
            mediaPlayer.start();

    }

    private void bambaraLangue(){
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer=MediaPlayer.create(this,R.raw.bambchoisi);
            mediaPlayer.start();
        }else {
            mediaPlayer=MediaPlayer.create(this,R.raw.bambchoisi);
            mediaPlayer.start();
        }

    }
    private void francaisLangue(){
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer=MediaPlayer.create(this,R.raw.franchoisi);
            mediaPlayer.start();
        }else {
            mediaPlayer=MediaPlayer.create(this,R.raw.franchoisi);
            mediaPlayer.start();
        }

    }



    /**
     * Display Progress bar while Logging in
     */

    private void displayLoader() {
        pDialog = new ProgressDialog(MainActivity.this);
        pDialog.setMessage("Cheicking electeur...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void loadElecteur(){
        displayLoader();
        JSONObject request = new JSONObject();
        try {
            //Populate the request parameters
            request.put("centre", user.getCentre());
            request.put("bureau", user.getBureau());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsArrayRequest = new JsonObjectRequest
                (Request.Method.POST, BASE_URL, request, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.dismiss();
                        try {
                            //Check if master got logged in successfully
                             message=response.getString(KEY_MESSAGE);

                            if (response.getInt(KEY_SUCCESS) == 1) {
                                JSONObject jsonObject=response.getJSONObject(KEY_DATA);
                                 id_Electeur = jsonObject.getString("id_Electeur");
                                 numElecteur = jsonObject.getString("numElecteur");
                                 numMatricule = jsonObject.getString("numMatricule");
                                 nom = jsonObject.getString("nom");
                                 prenom = jsonObject.getString("prenom");
                                 date_naissance = jsonObject.getString("date_naissance");
                                 lieu_naissance = jsonObject.getString("lieu_naissance");
                                 pays_nationalite = jsonObject.getString("pays_nationalite");
                                 parcours = jsonObject.getString("parcours");
                                 statut = jsonObject.getString("statut");
                                 genre = jsonObject.getString("genre");
                                 etat = jsonObject.getString("etat");
                                 id_bureau = jsonObject.getString("id_bureau");
                                 bureau = jsonObject.getString("bureau");
                                 centre = jsonObject.getString("centre");

                                Log.d("SESSION","id_bureau : "+id_bureau);
                                Log.d("SESSION","bureau : "+bureau);
                                Log.d("SESSION","centre : "+centre);


                                session.voteElecteur(id_Electeur,numElecteur,numMatricule,nom,prenom,date_naissance,lieu_naissance,pays_nationalite,parcours,statut,genre,etat,id_bureau,bureau,centre);

                                Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();

                               if(message==null){
                                   txtInfo.setText("Aucun électeur en cours !");
                                   txtElecteur.setText("--- Vide ! ---");
                               }else {
                                   txtInfo.setText(message);

                                   txtElecteur.setText(nom+" "+prenom+" né(e) le "+date_naissance);

                                   backgroundService = new BackgroundService(getCtx());
                                   mServiceIntent = new Intent(getCtx(), backgroundService.getClass());
                                   startService(mServiceIntent);

//
                               }

//

                            }else{
                                mediaPlayer.stop();
                                finish();
                                txtInfo.setText(message);
                                txtElecteur.setText("Erreur d'électeur");
                                Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();

                        //Display error message whenever an error occurs
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsArrayRequest);
    }



    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.d("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.d ("isMyServiceRunning?", false+"");
        return false;
    }


//    @Override
//    protected void onDestroy() {
//            stopService(mServiceIntent);
//            Log.d("MAINACT", "onDestroy!");
//            super.onDestroy();
//    }


    @Override
    public void onBackPressed() {
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
        }
        Intent i = new Intent(getApplicationContext(), InCommingActivity.class);
        startActivity(i);
        finish();
    }

}
