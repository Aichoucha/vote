<?php
/**
 * Created by PhpStorm.
 * User: AICHA
 * Date: 01/01/2018
 * Time: 03:30
 */

$response = array();
$userArray = array();

if(isset($_POST['id_Electeur_x'])&&isset($_POST['id_Electeur'])&&isset($_POST['sigle_parti'])){

    include '../db/db_connect.php';
    include '../db/fonctions.php';

	$id_Electeur_x = $_POST['id_Electeur_x'];
	$Electeur = $_POST['id_Electeur'];
    $bureau = $_POST['bureau'];
    //$sigle_parti = $_POST['sigle_parti'];
    $Candidat = $_POST['sigle_parti'];

//    $Candidat = id_Candidat($sigle_parti);

        
        
//        var_dump($Electeur,$sigle_parti,$Candidat,$avoter);exit();
        
        $query_insert = "INSERT INTO vote(id_Candidat,id_Electeur,bureau) VALUES (?,?,?)";
	//Query to verify
	$query_verify = "SELECT id_Electeur,id_Candidat FROM vote WHERE id_Electeur=?";
	if($stmt_select = $con->prepare($query_verify)){
		//Bind movie_id parameter to the query
		$stmt_select->bind_param("s",$id_Electeur_x);
		$stmt_select->execute();
		//Bind fetched result to variables id,reference,name,telephone,email,password,image,etat,role,date_adhesion
		$stmt_select->bind_result($id_Candidat,$id_Electeur);
		//Check for results	
//                var_dump($stmt_select->fetch());exit();
		if($stmt_select->fetch()){
			$response["success"] = 0;
			$response["message"] = "Electeur a deja vote";
			$response["data"] = $userArray;
		}else{

            $userArray["id_Candidat"] = $Candidat;
            $userArray["id_Electeur"] = $id_Electeur_x;
          //  $userArray["bureau"] = $bureau;
                
		if($stmt_insert=$con->prepare($query_insert)){
			//Bind parameters
			$stmt_insert->bind_param("sss",$Candidat,$id_Electeur_x,$bureau);
			//Exceting MySQL statement
			$stmt_insert->execute();
                        
			//Check if data got inserted
			if($stmt_insert->affected_rows == 1){
				
                 if(electeur_aVoter($Electeur)){
                     $response["success"] = 1;
                    $response["message"] = "Electeur a vote avec succes";
                    //$response["message"] = "Master inscris";
                     $userArray["bureau"] = $bureau;
                     $response["data"] = $userArray;
                 }else{
                     $response["success"] = 0;
                     $response["message"] = "Electeur n'a pas vote";
                     $response["data"] = $userArray;
                 }

			}else{
				//Some error while inserting
				$response["success"] = 0;
				$response["message"] = "Erreur de vote";
				$response["data"] = $userArray;
			}					
		}else{
			//Some error while inserting
			$response["success"] = 0;
			$response["message"] = "Erreur de serveur";
			$response["data"] = $userArray;
		}
                $stmt_insert->close();
		}
		
	}else{
		//Whe some error occurs
		$response["success"] = 0;
		$response["message"] = "Erreur de serveur";
		$response["data"] = $userArray;
		
	}
	$stmt_select->close();
	
}else{
	//Mandatory parameters are missing
	$response["success"] = 0;
	$response["message"] = "Parametre manquant";
        $response["data"] = $userArray;
}

//Displaying JSON response
echo json_encode($response);
?>