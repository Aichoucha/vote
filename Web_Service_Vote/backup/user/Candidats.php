<?php
/**
 * Created by PhpStorm.
 * User: AICHA
 * Date: 01/01/2018
 * Time: 03:30
 */

include '../db/db_connect.php';
//Query to select movie id and movie name
$result = array();
$electeurArray = array();
$response = array();

//Get the input request parameters

//Check for mandatory parameter
if(isset($_GET['id_Electeur'])){
 
$id_Electeur =$_GET['id_Electeur'];


    $query = "SELECT c.id_Candidat as id_Candidat, c.nom as nom, c.prenom as prenom,c.genre as genre, c.sigle_parti as sigle_parti, c.photo_Candidat as photo_Candidat FROM candidat c, electeur e WHERE e.id_Electeur='$id_Electeur' ORDER BY c.nom AND c.prenom";

if($stmt=$con->prepare($query)){
	$stmt->execute();
	$stmt->bind_result($id_Candidat,$nom,$prenom,$genre,$sigle_parti,$photo_Candidat);
	while($stmt->fetch()){
            //Populate the movie array
            $electeurArray["id_Candidat"] = $id_Candidat;
            $electeurArray["nom"] = $nom;
            $electeurArray["prenom"] = $prenom;
            $electeurArray["genre"] = $genre;
            $electeurArray["sigle_parti"] = $sigle_parti;

//            $electeurArray["photo"] = "http://10.0.2.2/Web_Service_Vote/img/".$photo_Candidat;
            $electeurArray["photo"] = "http://www.maliafrique.ml/Web_Service_Vote/img/".$photo_Candidat;
//            $electeurArray["photo_Candidat"] = '<img src="data:image/jpeg;base64,'.base64_encode($photo_Candidat).'"/>';
//            $electeurArray["photo_Candidat"] = base64_encode($photo_Candidat);

            $result[]=$electeurArray;
		
	}
        
	$response["success"] = 1;
	$response["data"] = $result;
	$response["message"] = "En cours de Listing";
//       var_dump($result);
//    exit();
        $stmt->close();
	

}else{
	//Some error while fetching data
	$response["success"] = 0;
        $response["data"] = $result;
	$response["message"] ="Erreur de serveur";
	
}


}else{
	//When the mandatory parameter movie_id is missing
	$response["status"] = 0;
        $response["data"] = $result;
	$response["message"] = "Parametre manquant";
        
}

//Display JSON response
echo json_encode($response);
//var_dump($response);
?>