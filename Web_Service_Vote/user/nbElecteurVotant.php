<?php


//Query to select movie id and movie name
$electeurArray = array();
$response = array();

//Get the input request parameters
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

//Check for mandatory parameter
if(isset($_GET['centre']) && isset($_GET['bureau'])){

    include '../db/db_connect.php';
    include '../db/fonctions.php';

    $centre =$_GET['centre'];
    $bureau =$_GET['bureau'];

    $nb_votant=getVotants($centre,$bureau);
    $nb_electeur=getNbElecteurs($centre,$bureau);


    if(! empty($nb_votant) && ! empty($nb_electeur)){

        $electeurArray["centre"] = $centre;
        $electeurArray["bureau"] = $bureau;
        $electeurArray["nb_electeur"] = $nb_electeur;
        $electeurArray["nb_votant"] = $nb_votant;

        $response["success"] = 1;

        $response["message"] = "En cours de Listing";
        $response["data"] = $electeurArray;

    }else{
        //Some error while fetching data
        $response["success"] = 0;
        $response["data"] = $electeurArray;
        $response["message"] ="Erreur de serveur";

    }


}else{
    //When the mandatory parameter movie_id is missing
    $response["status"] = 0;
    $response["data"] = $electeurArray;
    $response["message"] = "Parametre manquant";

}
//Display JSON response
echo json_encode($response);
//var_dump($response);
?>