package com.example.aicha.votesvel.helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Hashmap {


    // function to sort hashmap by values
    public static HashMap<String, String> sortByValue(HashMap<String, String> hm)
    {
        // Create a list from elements of HashMap
        List<Map.Entry<String, String> > list =
                new LinkedList<Map.Entry<String, String> >(hm.entrySet());

        // Sort the list
        Collections.sort(list, new Comparator<Map.Entry<String, String> >() {
            public int compare(Map.Entry<String, String> o1,
                               Map.Entry<String, String> o2)
            {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });

        // put data from sorted list to hashmap
        HashMap<String, String> temp = new LinkedHashMap<String, String>();
        for (Map.Entry<String, String> aa : list) {
            temp.put(aa.getKey(), aa.getValue().toString());
        }
        return temp;
    }

    public static HashMap<String, String> sortByValueArrayList(ArrayList<HashMap<String, String>> hmList)
    {
        // Create a list from elements of HashMap
        List<Map.Entry<String, String> > list;
        HashMap<String, String> temp = new LinkedHashMap<String, String>();

        for(int i=0;i<hmList.size();i++) {
            list = new LinkedList<Map.Entry<String, String> >(hmList.get(i).entrySet());

            Collections.sort(list, new Comparator<Map.Entry<String, String> >() {
                public int compare(
                        Map.Entry<String, String> o1,Map.Entry<String, String> o2)
                {
                    return (o2.getValue()).compareTo(o1.getValue());
                }
            });

            // put data from sorted list to hashmap
            for (Map.Entry<String, String> aa : list) {
                temp.put(aa.getKey(), aa.getValue().toString());
            }

        }
        return temp;
    }

    // function to sort hashmap by values
    public static ArrayList<HashMap<String, String>> sortByValueList(ArrayList<HashMap<String, Integer>> hmList)
    {
        // Create a list from elements of HashMap
        List<Map.Entry<String, Integer> > list;
        ArrayList<HashMap<String, String> > listTemp=new ArrayList<>();
        HashMap<String, String> temp = new LinkedHashMap<String, String>();

        for(int i=0;i<hmList.size();i++) {

            list = new LinkedList<Map.Entry<String, Integer> >(hmList.get(i).entrySet());

            Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() {
                public int compare(
                        Map.Entry<String, Integer> o1,Map.Entry<String, Integer> o2)
                {
                    return (o2.getValue()).compareTo(o1.getValue());
                }
            });

            // put data from sorted list to hashmap
            for (Map.Entry<String, Integer> aa : list) {
                temp.put(aa.getKey(), aa.getValue().toString());
            }

        }
        listTemp.add(temp);



        return listTemp;
    }




}
