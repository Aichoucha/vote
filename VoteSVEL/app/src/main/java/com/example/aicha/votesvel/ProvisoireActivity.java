package com.example.aicha.votesvel;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.example.aicha.votesvel.helper.ConfigHelper;
import com.example.aicha.votesvel.helper.HttpJsonParser;
import com.example.aicha.votesvel.helper.SessionHandler;
import com.example.aicha.votesvel.helper.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import com.example.aicha.votesvel.helper.RSAUtil;

import static com.example.aicha.votesvel.helper.Hashmap.sortByValue;
import static com.example.aicha.votesvel.helper.Hashmap.sortByValueArrayList;
import static com.example.aicha.votesvel.helper.Hashmap.sortByValueList;


public class ProvisoireActivity extends AppCompatActivity {


    private static final String KEY_SUCCESS = "success";
    private static final String KEY_DATA = "data";
    private static final String KEY_MESSAGE = "message";

    private static final String KEY_PARTI= "parti";
    private static final String KEY_ID_CANDIDAT= "id_Candidat";
    private static final String KEY_NOM= "nom";
    private static final String KEY_PRENOM= "prenom";
    private static final String KEY_GENRE= "genre";
    private static final String KEY_PHOTO= "photo";


    private static final String KEY_VOIX= "voix";
    private static final String KEY_ELECTEUR= "electeur";
    private static final String KEY_VONTANT= "votant";

    private static final String KEY_EMPTY = "";
    private static final String KEY_BUREAU= "bureau";
    private static final String KEY_CENTRE= "centre";

    String API_URL;
    String BASE_URL;
    String URL_CANDIDAT;


    private Button button;

    private String NumCandidat,NombreVotant,voix,pa,parti,photo,nom,prenom,bureau,centre,genre;

    public GridView gridview;

    private String [] app_prenom;
    private String [] app_nb;
    private String [] app_name;
    private String [] app_partie;
    private InputStream[] app_input;
    private String[] app_photo;

    private String nb_electeur,nb_votant,role,electeur,elect;

    private int success;
    private String message;
    private ProgressDialog pDialog;

    private SessionHandler session;
    User user;
    private Intent i;

    private Button btnElecteur,btnVotant,btnCalcul;
    private double taux;


    HashMap<String, String> mapTemp1;
    HashMap<String, String> mapVote;
    HashMap<String, String> mapCandidat;
    HashMap<String, String> mapTemp,mapTemp2,mapTemp3,mapTemp4;

    private ArrayList<HashMap<String, String>> VoteList;
    private ArrayList<HashMap<String, String>> candidatList;

    HashMap<String, String> mapVoix;
    HashMap<String, String> mapVoixOrder;
    HashMap<String, String> mapName;
    HashMap<String, String> mapPhoto;

    private ArrayList<HashMap<String, String>> mapNameList;
    private ArrayList<HashMap<String, String>> mapPhotoList;
    private ArrayList<HashMap<String, String>> mapVoixList;
    private HashMap<String, String> Order;
    private HashMap<String, String> mapVoixListOrder;
    private ArrayList<HashMap<String, String>> tempVoteList;
    private ArrayList<HashMap<String, String>> mapVoteList;
    private ArrayList<HashMap<String, String>> mapVoixOrderList;

    int nbElecteurVotant;
    Context ctx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provisoire);
        ctx=this;

        API_URL = ConfigHelper.getConfigValue(this,"api_url");
        BASE_URL = API_URL+"resultatprosoire.php";
        URL_CANDIDAT = API_URL+"getAllCandidats.php";

        nbElecteurVotant=0;
        session = new SessionHandler(getApplicationContext());
        user = session.getUserDetails();


        Intent get_intent=getIntent();

        if(get_intent.getStringExtra("bureau")!=null && get_intent.getStringExtra("centre")!=null){
//            get_id_bureau=get_intent.getStringExtra("id_bureau");
            bureau=get_intent.getStringExtra("bureau");
            centre=get_intent.getStringExtra("centre");
        }else{
            centre=user.getCentre();
            bureau=user.getBureau();
        }



        setTitle("Provisoires - "+centre+" : "+bureau);
        if(gridview==null){
            gridview=(GridView)findViewById(R.id.gridView);
        }

        new FetchAllCandidatsAsyncTask().execute();


    }


    /**
     * Fetches the list of appros from the server
     */
    private class FetchResultatAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display progress bar
            pDialog = new ProgressDialog(ProvisoireActivity.this);
            pDialog.setMessage("Calcul en cours...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected String doInBackground(String... params) {
            HttpJsonParser httpJsonParser = new HttpJsonParser();
            Map<String, String> httpParams = new HashMap<>();

            httpParams.put(KEY_CENTRE, centre);
            httpParams.put(KEY_BUREAU, bureau);


            JSONObject jsonObject = httpJsonParser.makeHttpRequest(
                    BASE_URL, "GET", httpParams);
            try {
                int success = jsonObject.getInt(KEY_SUCCESS);
                JSONArray apppros;

                if (success == 1) {
                    VoteList = new ArrayList<>();

                    apppros = jsonObject.getJSONArray(KEY_DATA);

                    for (int i = 0; i < apppros.length(); i++) {

                        JSONObject p = apppros.getJSONObject(i);
                        nbElecteurVotant++;
                        pa = p.getString(KEY_PARTI);
//                        elect = p.getString(KEY_ELECTEUR);
                        parti=RSAUtil.decrypt(ctx,pa);
//                        electeur=RSAUtil.decrypt(elect);
                        voix = "0";
                        centre = p.getString(KEY_CENTRE);
                        bureau = p.getString(KEY_BUREAU);

                        Log.d("vote","parti : "+i+" "+pa);

                        mapVote = new HashMap<String, String>();
//                        mapVote.put(parti,"0");
                        mapVote.put(parti, voix);

                        VoteList.add(mapVote);
                    }
                }else {
                    Toast.makeText(getApplicationContext(),
                            jsonObject.getString(KEY_MESSAGE), Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    populateApproList();
                }
            });
        }

    }

    private class FetchAllCandidatsAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display progress bar
            pDialog = new ProgressDialog(ProvisoireActivity.this);
            pDialog.setMessage("Affichage en cours...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpJsonParser httpJsonParser = new HttpJsonParser();

            Map<String, String> httpParams = new HashMap<>();
            httpParams.put("getAllCandidats", "getAllCandidats");


            JSONObject jsonObject = httpJsonParser.makeHttpRequest(
                    URL_CANDIDAT, "GET", httpParams);
            try {
                int success = jsonObject.getInt(KEY_SUCCESS);
                JSONArray apppros;

                if (success == 1) {

                    candidatList = new ArrayList<>();

                    mapNameList = new ArrayList<>();
                    mapVoixList = new ArrayList<>();
                    mapPhotoList = new ArrayList<>();

                    apppros = jsonObject.getJSONArray(KEY_DATA);

                    app_prenom=new String[apppros.length()];
                    app_nb=new String[apppros.length()];
                    app_name=new String[apppros.length()];
                    app_partie=new String[apppros.length()];
                    app_photo=new String[apppros.length()];
                    app_input=new InputStream[apppros.length()];

                    byte[] Photodata;
                    InputStream inputStream;
                    //Iterate through the response and populate appros list
                    for (int i = 0; i < apppros.length(); i++) {
                        JSONObject approvisionnement = apppros.getJSONObject(i);

                        NumCandidat = approvisionnement.getString(KEY_ID_CANDIDAT);
                        nom = approvisionnement.getString(KEY_NOM);
                        prenom = approvisionnement.getString(KEY_PRENOM);
                        genre = approvisionnement.getString(KEY_GENRE);
                        photo = approvisionnement.getString(KEY_PHOTO);
                        parti = approvisionnement.getString(KEY_PARTI);

                        voix="0";

//                        app_prenom[i]=nom+" "+prenom;
//                        app_nb[i]=voix+" Voix soit 0 %";
//                        app_partie[i]=parti;
//                        app_photo[i]=photo;

                        mapCandidat = new HashMap<String, String>();

                        mapVoix = new HashMap<String, String>();
                        mapName = new HashMap<String, String>();
                        mapPhoto = new HashMap<String, String>();

                        mapCandidat.put(KEY_PHOTO, photo);
                        mapCandidat.put(parti, voix);
                        mapCandidat.put(KEY_PRENOM, nom+" "+prenom);

                        candidatList.add(mapCandidat);



                        mapPhoto.put(parti, photo);
                        mapVoix.put(parti, voix);
                        mapName.put(parti, nom+" "+prenom);

                        mapNameList.add(mapName);

                        mapPhotoList.add(mapPhoto);
                        mapVoixList.add(mapVoix);
                    }

                }else {
                    Toast.makeText(getApplicationContext(),
                            jsonObject.getString(KEY_MESSAGE), Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {

                    new FetchResultatAsyncTask().execute();

                }
            });
        }

    }


    /**
     * Updating parsed JSON data into ListView
     * */
    private void populateApproList() {

        Log.d("aicha","VoteList");
        if(VoteList.size()>0){
            for(int j=0;j<VoteList.size();j++){
                mapTemp3=VoteList.get(j);
                for(int k=0;k<VoteList.size();k++){
                    mapTemp4=VoteList.get(k);
                    for (Map.Entry<String, String> candidat : mapTemp3.entrySet()){
                        for (Map.Entry<String, String> vote : mapTemp4.entrySet()){
                            if(candidat.getKey().equals(vote.getKey())){
                                int nb=Integer.parseInt(candidat.getValue())+1;
                                candidat.setValue(String.valueOf(nb));
                            }
                        }
                    }
                }
            }
        }


        for(int i=0;i<mapVoixList.size();i++){
            mapTemp1=mapVoixList.get(i);
            if(VoteList.size()>0){
                for(int j=0;j<VoteList.size();j++){
                    mapTemp2=VoteList.get(j);
                    for (Map.Entry<String, String> candidat : mapTemp1.entrySet()){
                        for (Map.Entry<String, String> vote : mapTemp2.entrySet()){
                            if(candidat.getKey().equals(vote.getKey())){
                                candidat.setValue(vote.getValue());
                            }

                        }
                    }
                }
            }
        }


        mapVoixListOrder=sortByValueArrayList(mapVoixList);
        Log.d("order","avant ordonner");
            HashMap<String,String> test=mapVoixListOrder;
            for(Map.Entry<String,String> maptest:test.entrySet()){
                Log.d("order","key : "+maptest.getKey()+"  value : "+maptest.getValue());

            }

            mapVoixListOrder=sortByValue(mapVoixListOrder);
            Log.d("order","apres ordonner");
            HashMap<String,String> oderMapVoix=mapVoixListOrder;
            int fst=0;
            for(Map.Entry<String,String> voix:oderMapVoix.entrySet()){
                Log.d("order","HashMap " +fst+ " key : "+voix.getKey()+"  value : "+voix.getValue());

                app_partie[fst]=voix.getKey();
                double p=(Double.parseDouble(voix.getValue())*100)/(nbElecteurVotant);
                app_nb[fst]=voix.getValue()+" Voix";

                if(String.valueOf(p).length()>=5){
                    app_name[fst]=String.valueOf(p).substring(0,5)+" %";
                }else{
                    app_name[fst]=p+" %";
                }

                for(int q=0;q<mapNameList.size();q++){
                    HashMap<String,String> oderMapName=mapNameList.get(q);
                    for(Map.Entry<String,String> name:oderMapName.entrySet()){
                        if(name.getKey().equals(voix.getKey())){
                            app_prenom[fst]=name.getValue();
                        }
                    }
                }

                for(int t=0;t<mapPhotoList.size();t++){
                    HashMap<String,String> oderMapPhoto=mapPhotoList.get(t);
                    for(Map.Entry<String,String> photo:oderMapPhoto.entrySet()){
                        if(photo.getKey().equals(voix.getKey())){
                            app_photo[fst]=photo.getValue();
                        }
                    }
                }

                fst++;
            }

        // setting up Adapter tp GridView
        if(gridview.getAdapter()==null){
            gridview.setAdapter(new GridViewAdapter(ProvisoireActivity.this,app_prenom,app_nb,app_name,app_partie,app_photo));
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 20) {
            // If the result code is 20 that means that
            // the master has deleted/updated the movie.
            // So refresh the movie listing
            Intent intent = getIntent();
            finish();
            startActivity(intent);
        }
    }


    public void afficheMap(Map<String, String> map) {
        for (Map.Entry<String, String> contact : map.entrySet())
            Log.d("order","key : "+contact.getKey()+" value : "+contact.getValue());
    }

    public void afficheMap2(Map<String, String> map) {
        for (Map.Entry<String, String> contact : map.entrySet())
            Log.d("order","key : "+contact.getKey()+" value : "+contact.getValue());
    }




}
