<?php
/**
 * Created by PhpStorm.
 * User: TIEZ
 * Date: 02/12/2018
 * Time: 03:30
 */


$response = array();
$userArray = array();

include '../db/db_connect.php';
include '../db/fonctions.php';


if(isset($_POST['numElecteur']) && isset($_POST['mmesamake']) ){


    $numElecteur =$_POST['numElecteur'];
    $etat="VEUT VOTER";
//            $query_insert = "UPDATE electeur SET etat=? WHERE etat!='A VOTER' AND numElecteur=?";
    $query_insert = "UPDATE electeur SET etat=? WHERE numElecteur=?";

    $userArray["numElecteur"] = $numElecteur;

    if($stmt_insert=$con->prepare($query_insert)){
        //Bind parameters
        $stmt_insert->bind_param("ss",$etat,$numElecteur);
        //Exceting MySQL statement
        $stmt_insert->execute();

        //Check if data got inserted
        if($stmt_insert->affected_rows == 1){
            $response["success"] = 1;
            $response["message"] = "Le numero d'électeur $numElecteur est autorisé à voter";
            $response["data"] = $userArray;


        }else{
            //Some error while inserting
            $response["success"] = 0;
            $response["message"] = "L'électeur $numElecteur n'est pas autorisé à voter";
            $response["data"] = $userArray;
        }
    }else{
        //Some error while inserting
        $response["success"] = 0;
        $response["message"] ="Erreur du système";
        $response["data"] = $userArray;
    }


    $stmt_insert->close();


}else{
    //Mandatory parameters are missing
    $response["success"] = 0;
    $response["message"] = "Parametre manquant";
    $response["data"] = $userArray;
}

//Displaying JSON response
echo json_encode($response);
?>