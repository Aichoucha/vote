package com.example.hp.svelmobile;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.hp.svelmobile.helper.BackgroundService;
import com.example.hp.svelmobile.helper.SessionHandler;
import com.example.hp.svelmobile.helper.User;

import java.util.Date;

import static com.example.hp.svelmobile.helper.User.mediaPlayer;

public class InCommingActivity extends AppCompatActivity {

    private Date sessionExpiryDate;
    private  long expires;

    Button button,button3;
    SessionHandler session;
    User user;
    public static boolean boolRest;

    public static boolean isBoolRest() {
        return boolRest;
    }

    public static void setBoolRest(boolean a) {
        boolRest = a;
    }

//    Intent smsServiceIntent;
//    BackgroundService backgroundService;
//    Intent mServiceIntent;
//    Context ctx;
//    public Context getCtx() {
//        return ctx;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_comming);

        button=findViewById(R.id.button);
        button3=findViewById(R.id.button3);

        session=new SessionHandler(getApplicationContext());
        user=session.getUserDetails();
        sessionExpiryDate=user.getSessionExpiryDate();
        setTitle("Commencer vote au "+user.getBureau());

        button3.setText(user.getCentre()+" : "+user.getBureau());

        button.setText("COMMENCER");

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             Intent i=new Intent(InCommingActivity.this, MainActivity.class);
                    setBoolRest(true);
                    startActivity(i);
            }
        });
    }

    @Override
    public void onBackPressed() {

        Intent intent=new Intent(getApplicationContext(),InCommingActivity.class);
        startActivity(intent);
        finish();
    }

}
