package com.example.hp.svelmobile;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.hp.svelmobile.helper.CheckNetworkStatus;
import com.example.hp.svelmobile.helper.ConfigHelper;
import com.example.hp.svelmobile.helper.MySingleton;
import com.example.hp.svelmobile.helper.SessionHandler;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity  {
    private static final String KEY_EMPTY = "";
    private static final String KEY_DATA = "data";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_SUCCESS = "success";
    private static final String KEY_ID_USER = "idUtilisteur";
    private static final String KEY_LOGIN = "login";
    private static final String KEY_PWD = "pwd";
    private static final String KEY_TELEPHONE = "telephone";
    private static final String KEY_NAME = "name";
    private static final String KEY_ROLE= "role";
    private static final String KEY_ID_BUREAU= "id_bureau";
    private static final String KEY_BUREAU= "bureau";
    private static final String KEY_CENTRE= "centre";

    String API_URL;
    String BASE_URL;

    private String idUtilisteur,login,pwd,telephone,name,role,id_bureau,bureau,centre,username,password;

    private int success;
    private String message;
    private ProgressDialog pDialog;

    private Button btnConnexion;
    EditText edtLogin, edtPwd;
    TextView textView4;

    private SessionHandler session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        API_URL = ConfigHelper.getConfigValue(this,"api_url");
        BASE_URL = API_URL+"getUser.php";

        setTitle("Authentification - SVEL");

        textView4=findViewById(R.id.textView4);
        session = new SessionHandler(getApplicationContext());

        edtLogin=findViewById(R.id.login);
        edtPwd=findViewById(R.id.password);

        btnConnexion=findViewById(R.id.btbcon);
        btnConnexion.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                //Retrieve the data entered in the edit texts
                username = edtLogin.getText().toString().toLowerCase().trim();
                password = edtPwd.getText().toString().trim();
                if (validateInputs()) {
                    if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {
                        login_connexion();
                    }
                    else {
                        Toast.makeText(LoginActivity.this,
                                "Impossible de se connecter à l'internet",
                                Toast.LENGTH_LONG).show();

                    }

                }

            }
        });


}


    /**
     * Launch Dashboard Activity on Successful Login
     */
    private void loadDashboard() {
        Intent i = new Intent(getApplicationContext(), InCommingActivity.class);
        startActivity(i);
        finish();

    }

    /**
     * Display Progress bar while Logging in
     */

    private void displayLoader() {
        pDialog = new ProgressDialog(LoginActivity.this);
        pDialog.setMessage("Connexion en cours...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }


    private void login_connexion() {
        displayLoader();
        JSONObject request = new JSONObject();
        try {
            //Populate the request parameters
            request.put(KEY_LOGIN, username);
            request.put(KEY_PWD, password);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsArrayRequest = new JsonObjectRequest
                (Request.Method.POST, BASE_URL, request, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.dismiss();
                        try {
                            //Check if master got logged in successfully

                            if (response.getInt(KEY_SUCCESS) == 1) {
                                JSONObject jsonObject=response.getJSONObject(KEY_DATA);
//                              idUtilisteur,login,pwd,telephone,name,role,id_bureau,bureau
                                idUtilisteur = jsonObject.getString("idUtilisteur");
                                login = jsonObject.getString("login");
                                pwd = jsonObject.getString("pwd");
                                telephone = jsonObject.getString("telephone");
                                name = jsonObject.getString("name");
                                role = jsonObject.getString("role");
                                id_bureau = jsonObject.getString("id_bureau");
                                bureau = jsonObject.getString("bureau");
                                centre = jsonObject.getString("centre");

                                Log.d("SESSION","idUtilisteur: "+idUtilisteur);
                                Log.d("SESSION","login : "+login);
                                Log.d("SESSION","pwd : "+pwd);
                                Log.d("SESSION","telephone : "+telephone);
                                Log.d("SESSION","name : "+name);
                                Log.d("SESSION","role : "+role);
                                Log.d("SESSION","id_bureau : "+id_bureau);
                                Log.d("SESSION","bureau : "+bureau);
                                Log.d("SESSION","centre : "+centre);

                                if(role.equals("PRESIDENTB")){
                                    session.loginUser(idUtilisteur,login,pwd,telephone,name,role,id_bureau,bureau,centre);
                                    Toast.makeText(getApplicationContext(),response.getString(KEY_MESSAGE), Toast.LENGTH_LONG).show();
                                    loadDashboard();
                                }else{
                                    textView4.setText("Vous n'êtes pas un president de bureau. Accès non autorisé");
                                    Toast.makeText(getApplicationContext(),"Vous n'êtes pas un president de bureau. Accès non autorisé", Toast.LENGTH_LONG).show();
                                }





                            }else{

                                    Toast.makeText(getApplicationContext(),response.getString(KEY_MESSAGE), Toast.LENGTH_LONG).show();



                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();

                        //Display error message whenever an error occurs
                        Toast.makeText(getApplicationContext(),
                                error.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsArrayRequest);
    }


    private boolean validateInputs() {
        if(KEY_EMPTY.equals(edtLogin.getText().toString())){
            edtLogin.setError("Nom d'utilisateur obligatoire");
            edtLogin.requestFocus();
            return false;
        }

        if(KEY_EMPTY.equals(edtPwd.getText().toString())){
            edtPwd.setError("Mot de passe obligatoire");
            edtPwd.requestFocus();
            return false;
        }
        return true;
    }

}

