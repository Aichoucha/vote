package com.example.hp.svelmobile;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

public class SplashScreenActivity extends AppCompatActivity {
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 4000;  // 5 sec
    TextView txtZoomIn;

    Animation animZoomIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        setTitle("Splash");

     //   animZoomIn = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.zoom_in);
     //   txtZoomIn=findViewById(R.id.txtZoomIn);
     //   txtZoomIn.setVisibility(View.VISIBLE);
     //   txtZoomIn.startAnimation(animZoomIn);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // Cette méthode sera exécutée une fois le délai écoulé.
                // Démarrer l'activité principale de votre application
                Intent i = new Intent(SplashScreenActivity.this, LoginActivity.class);
                startActivity(i);

                // fermer cette activité
                finish();
            }
        }, SPLASH_TIME_OUT);

    }

}
