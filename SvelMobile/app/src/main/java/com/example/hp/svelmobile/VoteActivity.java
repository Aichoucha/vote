package com.example.hp.svelmobile;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.example.hp.svelmobile.helper.CheckNetworkStatus;
import com.example.hp.svelmobile.helper.ConfigHelper;
import com.example.hp.svelmobile.helper.HttpJsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.hp.svelmobile.helper.User.mediaPlayer;

public class VoteActivity extends AppCompatActivity {

    private static final String KEY_SUCCESS = "success";
    private static final String KEY_DATA = "data";
    private static final String KEY_ID = "id_Candidat";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_NOM = "nom";
    private static final String KEY_PRENNOM = "prenom";
    private static final String KEY_GENREE= "genre";
    private static final String KEY_PARTI= "sigle_parti";
    private static final String KEY_PHOTO= "photo";
    private static final String KEY_DATE= "date";
    private static final String KEY_IDELECTEUR= "id_Electeur";
    private static final String KEY_ID_BUREAU= "id_bureau";
    private static final String KEY_BUREAU = "bureau";
    private static final String KEY_CENTRE = "centre";

    String API_URL;
    String BASE_URL;

    private ArrayList<HashMap<String, String>> candidatList;
//    private ListView candidatsListView;
    private ProgressDialog pDialog;

    private Button button;

    private String id_Candidat,id_Electeur,nom,prenom,genre,sigle_parti,photo_Candidat,  id_bureau,bureau,centre;
    String langue,e_idElecteur,e_numElecteur,e_numMatricule,e_nom,e_prenom,e_date_naissance,e_lieu_naissance,e_pays_nationalite,e_parcours,e_statut,e_genre,e_etat,e_id_bureau,e_bureau,e_centre;

    public GridView gridview;
//    private static String [] app_name = {"sample_0", "sample_1", "sample_2", "sample_3", "sample_4", "sample_5", "sample_6", "sample_7"};



//    private static int [] app_icon = {
//            R.drawable.sample_0, R.drawable.sample_1, R.drawable.sample_2, R.drawable.sample_3,R.drawable.sample_4, R.drawable.sample_5, R.drawable.sample_6, R.drawable.sample_7
//    };

//    private int [] app_icon;
    private String [] app_name;
    private String [] app_partie;
//    private InputStream[] app_input;
    private String[] app_photo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vote);
        API_URL = ConfigHelper.getConfigValue(this,"api_url");
        BASE_URL = API_URL+"Candidats.php";

//        candidatsListView = (ListView) findViewById(R.id.candidatsListe);


        if(gridview==null){
            gridview=(GridView)findViewById(R.id.gridView);
        }

        Intent i =getIntent();
        langue=i.getStringExtra("langue");
        e_idElecteur=i.getStringExtra("idElecteur");
        e_numElecteur=i.getStringExtra("numElecteur");
        e_numMatricule=i.getStringExtra("numMatricule");
        e_nom=i.getStringExtra("nom");
        e_prenom=i.getStringExtra("prenom");
        e_date_naissance=i.getStringExtra("date_naissance");
        e_lieu_naissance=i.getStringExtra("lieu_naissance");
        e_pays_nationalite=i.getStringExtra("pays_nationalite");
        e_statut=i.getStringExtra("statut");
        e_parcours=i.getStringExtra("parcours");
        e_genre=i.getStringExtra("genre");

        e_etat=i.getStringExtra("etat");
//        e_id_bureau,e_bureau,e_centre;

        e_id_bureau=i.getStringExtra("id_bureau");
        e_bureau=i.getStringExtra("bureau");
        e_centre=i.getStringExtra("centre");

        setTitle("SVEL - "+e_prenom+" "+e_nom+" "+e_date_naissance);

        Log.d("getIntent","getIntent:"+i.getExtras().toString());
        new FetchCandidatsAsyncTask().execute();

        // casting xml GridView into javacode

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(final AdapterView<?> parent, final View v, final int position, long id) {


//                new android.support.v7.app.AlertDialog.Builder(VoteActivity.this)
//                        .setTitle("Vous allez voter pour "+app_partie[position]+" de "+app_name[position]+" !")
//                        .setMessage("êtes-vous sûr de votre choix ?")
//                        .setPositiveButton("Oui, Choisir", new DialogInterface.OnClickListener() {
//                            @RequiresApi(api = Build.VERSION_CODES.M)
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {

                                Toast.makeText(VoteActivity.this,"Vous avez choisi de voter pour "+app_name[position], Toast.LENGTH_LONG).show();

                                Intent i = new Intent(getApplicationContext(), Voter.class);
                                i.putExtra("langue",langue);
                                i.putExtra("idElecteur",e_idElecteur);
                                i.putExtra("numElecteur",e_numElecteur);
                                i.putExtra("numMatricule",e_numMatricule);
                                i.putExtra("nom",e_nom);
                                i.putExtra("prenom",e_prenom);
                                i.putExtra("date_naissance",e_date_naissance);
                                i.putExtra("lieu_naissance",e_lieu_naissance);
                                i.putExtra("pays_nationalite",e_pays_nationalite);
                                i.putExtra("parcours",e_parcours);
                                i.putExtra("statut",e_statut);
                                i.putExtra("genre",e_genre);
                                i.putExtra("etat",e_etat);

                                i.putExtra("id_bureau",e_id_bureau);
                                i.putExtra("centre",e_bureau);
                                i.putExtra("centre",e_centre);


                                i.putExtra("candidat",app_name[position]);
                                i.putExtra("partie",app_partie[position]);
                                i.putExtra("photo",app_photo[position]);
                                startActivity(i);
                                finish();
//                            }
//                        })
//                        .setNegativeButton("Non, ne pas Choisir", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                Toast.makeText(VoteActivity.this,"Vous avez annulé votre choix de "+app_name[position], Toast.LENGTH_LONG).show();
//                            }
//                        })
//                        .show();
            }
        });


    }

    /**
     * Fetches the list of appros from the server
     */
    private class FetchCandidatsAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display progress bar
            pDialog = new ProgressDialog(VoteActivity.this);
            pDialog.setMessage("Listing des candidats...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            HttpJsonParser httpJsonParser = new HttpJsonParser();
            Map<String, String> httpParams = new HashMap<>();
            httpParams.put(KEY_IDELECTEUR, e_idElecteur);

            Log.d(KEY_IDELECTEUR,KEY_IDELECTEUR+":"+e_idElecteur);

            JSONObject jsonObject = httpJsonParser.makeHttpRequest(
                    BASE_URL, "GET", httpParams);
            try {
                int success = jsonObject.getInt(KEY_SUCCESS);
                JSONArray apppros;

                if (success == 1) {
                    candidatList = new ArrayList<>();

                    apppros = jsonObject.getJSONArray(KEY_DATA);

                    app_name=new String[apppros.length()];
                    app_partie=new String[apppros.length()];
                    app_photo=new String[apppros.length()];
//                    app_input=new InputStream[apppros.length()];

                    byte[] Photodata;
                    InputStream inputStream;
                    //Iterate through the response and populate appros list
                    for (int i = 0; i < apppros.length(); i++) {
                        JSONObject approvisionnement = apppros.getJSONObject(i);
                        id_Candidat = approvisionnement.getString(KEY_ID);
                        nom = approvisionnement.getString(KEY_NOM);
                        prenom = approvisionnement.getString(KEY_PRENNOM);
                        sigle_parti = approvisionnement.getString(KEY_PARTI);
                        photo_Candidat = approvisionnement.getString(KEY_PHOTO);
                        Log.d("photo_Candidat","photo_Candidat : "+i+"_"+photo_Candidat);

                        app_name[i]=prenom+"  "+nom;
                        app_partie[i]=sigle_parti;
                        app_photo[i]=photo_Candidat;
//                        app_input[i]=new ByteArrayInputStream(Base64.decode(approvisionnement.getString(KEY_PHOTO).getBytes(), 0));

                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put(KEY_NOM, prenom+"  "+nom);
                        map.put(KEY_PHOTO, photo_Candidat);
                        map.put(KEY_PARTI, sigle_parti);

                        candidatList.add(map);
                    }
                }else {
                    Toast.makeText(getApplicationContext(),
                            jsonObject.getString(KEY_MESSAGE), Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    populateApproList();
                }
            });
        }

    }

    /**
     * Updating parsed JSON data into ListView
     * */
    private void populateApproList() {
        // setting up Adapter tp GridView
        gridview.setAdapter(new GridViewAdapter(VoteActivity.this,app_name,app_partie,app_photo));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 20) {
            // If the result code is 20 that means that
            // the master has deleted/updated the movie.
            // So refresh the movie listing
            if(mediaPlayer.isPlaying()){
                mediaPlayer.stop();
            }
            Intent intent = getIntent();

            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
        }
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
//        finish();
    }

}