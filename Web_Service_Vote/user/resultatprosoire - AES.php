<?php
/**
 * Created by PhpStorm.
 * User: AICHA
 * Date: 01/01/2018
 * Time: 03:30
 */

include '../db/db_connect.php';
//Query to select movie id and movie name
$result = array();
$electeurArray = array();
$response = array();

//Get the input request parameters

//Check for mandatory parameter
if(isset($_GET['bureau']) && isset($_GET['centre'])){

$centre =$_GET['centre'];
$bureau =$_GET['bureau'];

$query = "SELECT 
v.id_Candidat as sigle_parti, 
count(v.id_Candidat) as voix 
from vote v, bureau b, centre c
WHERE 
v.bureau=b.id AND 
b.centre_id=c.id AND 
c.nom_centre='$centre' AND b.nom_bureau='$bureau'
GROUP BY sigle_parti ORDER BY sigle_parti DESC";

if($stmt=$con->prepare($query)){
	$stmt->execute();
	$stmt->bind_result($sigle_parti,$voix);
	while($stmt->fetch()){
            //Populate the movie array
            $electeurArray["parti"] = $sigle_parti;
            $electeurArray["voix"] = $voix;
            $electeurArray["bureau"] = $bureau;
            $electeurArray["centre"] = $centre;

            $result[]=$electeurArray;
		
	}
        
	$response["success"] = 1;
	$response["data"] = $result;
	$response["message"] = "En cours de Listing";
//       var_dump($result);
//    exit();
        $stmt->close();
	

}else{
	//Some error while fetching data
	$response["success"] = 0;
        $response["data"] = $result;
	$response["message"] ="Erreur de serveur";
	
}


}else{
	//When the mandatory parameter movie_id is missing
	$response["status"] = 0;
        $response["data"] = $result;
	$response["message"] = "Parametre manquant";

}
//Display JSON response
echo json_encode($response);
//var_dump($response);
?>