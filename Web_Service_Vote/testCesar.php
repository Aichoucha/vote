<?php

require ('./CaesarCipher.php');

$cipher = new CaesarCipher(1);
echo $cipher->encrypt("hello world");
echo $cipher->encrypt("HELLO WORLD");
echo $cipher->encrypt("Hello World");
$cipher = new CaesarCipher(5);
echo $cipher->encrypt("hello world");
echo $cipher->encrypt("HELLO WORLD");
echo $cipher->encrypt("Hello World");
$cipher = new CaesarCipher(8);
echo $cipher->encrypt("hello world");
echo $cipher->encrypt("HELLO WORLD");
echo $cipher->encrypt("Hello World");
$cipher = new CaesarCipher(13);
echo $cipher->encrypt("hello world");
echo $cipher->encrypt("HELLO WORLD");
echo $cipher->encrypt("Hello World");
$cipher = new CaesarCipher(20);
echo $cipher->encrypt("hello world");
echo $cipher->encrypt("HELLO WORLD");
echo $cipher->encrypt("Hello World");
$cipher = new CaesarCipher(25);
echo $cipher->encrypt("hello world");
echo $cipher->encrypt("HELLO WORLD");
echo $cipher->encrypt("Hello World");
$cipher = new CaesarCipher(30);
echo $cipher->encrypt("hello world");
echo $cipher->encrypt("HELLO WORLD");
echo $cipher->encrypt("Hello World");




?>