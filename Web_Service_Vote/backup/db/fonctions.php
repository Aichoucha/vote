<?php
  
function getNbElecteurs($centre,$bureau){
    $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);      
    $query = "SELECT count(e.id_Electeur) as nb_electeur
              FROM electeur e, bureau b ,centre c
              WHERE e.id_bureau=b.id  and b.centre_id=c.id AND c.nom_centre=? AND b.nom_bureau=? GROUP BY e.id_bureau";
    if($stmt = $con->prepare($query)){
        //Bind movie_id parameter to the query
        $stmt->bind_param("ss",$centre,$bureau);
        $stmt->execute();
        $stmt->bind_result($nb_electeur);
        if($stmt->fetch()){
            $id=$nb_electeur;
            
        }
        $stmt->close();
    }
    return $id;
   }


function getNontVotants($centre,$bureau){
    $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);
    $query =  "SELECT count(e.id_Electeur) as nb_votant
    FROM electeur e, bureau b ,centre c
    WHERE e.id_bureau=b.id  and b.centre_id=c.id AND e.etat='PAS VOTER' AND c.nom_centre=? AND b.nom_bureau=? GROUP BY e.id_bureau";
    if($stmt = $con->prepare($query)){
        //Bind movie_id parameter to the query
        $stmt->bind_param("ss",$centre,$bureau);
        $stmt->execute();
        $stmt->bind_result($nb_votant);
        if($stmt->fetch()){
            $id=$nb_votant;

        }
        $stmt->close();
    }
    return $id;
}


function id_Candidat($sigle_parti){
    $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);
    $query = "SELECT id_Candidat FROM candidat WHERE sigle_parti=?";
    if($stmt = $con->prepare($query)){
        //Bind movie_id parameter to the query
        $stmt->bind_param("s",$sigle_parti);
        $stmt->execute();
        $stmt->bind_result($id_Candidat);
        if($stmt->fetch()){
            $id=$id_Candidat;

        }
        $stmt->close();
    }
    return $id;
}
   
function electeur_aVoter($id_Electeur){ 
    $bool=false;
    $con = mysqli_connect(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);      
    $query = "UPDATE electeur SET etat='A VOTE' WHERE id_Electeur=?";
    if($stmt = $con->prepare($query)){
        //Bind movie_id parameter to the query
        $stmt->bind_param("s",$id_Electeur);
        $stmt->execute();
        if($stmt->affected_rows == 1){
            $bool=true;
        }
        
        $stmt->close();
    }
    return $bool;
   }
   
function format_date($original='', $format="%d-%m-%Y") { 
    $format = ($format=='date' ? "%m-%d-%Y" : $format); 
    $format = ($format=='datetime' ? "%m-%d-%Y %H:%M:%S" : $format); 
    $format = ($format=='mysql-date' ? "%Y-%m-%d" : $format); 
    $format = ($format=='mysql-datetime' ? "%Y-%m-%d %H:%M:%S" : $format); 
    return (!empty($original) ? strftime($format, strtotime($original)) : "" );
    
}

function format_date_time($original='', $format="%d-%m-%Y %H:%M:%S") { 
    $format = ($format=='date' ? "%m-%d-%Y" : $format); 
    $format = ($format=='datetime' ? "%m-%d-%Y %H:%M:%S" : $format); 
    $format = ($format=='mysql-date' ? "%Y-%m-%d" : $format); 
    $format = ($format=='mysql-datetime' ? "%Y-%m-%d %H:%M:%S" : $format); 
    return (!empty($original) ? strftime($format, strtotime($original)) : "" );
    
}




      ?>
   