<?php

$userArray = array();
$response = array();

//Get the input request parameters
$inputJSON = file_get_contents('php://input');
$input = json_decode($inputJSON, TRUE); //convert JSON into array

//Check for mandatory parameter movie_id
if( isset($input['login']) && isset($input['pwd'])){

    include '../db/db_connect.php';
    include '../db/fonctions.php';

    $identifiant = $input['login'];
    $password = $input['pwd'];

    
    $query = "SELECT 
        u.idUtilisteur as idUtilisteur,u.login as login,u.pwd as pwd,u.telephone as telephone,u.name as name,u.role as role,u.id_bureau as id_bureau,b.nom_bureau as bureau,c.nom_centre as centre
        FROM utilisateur u, bureau b, centre c
        WHERE u.id_bureau=b.id AND b.centre_id=c.id AND login=? AND pwd=?";
    if($stmt = $con->prepare($query)){
        //Bind movie_id parameter to the query
        $stmt->bind_param("ss",$identifiant,$password);
        $stmt->execute();
        //Bind fetched result to variables id,reference,name,telephone,email,password,image,etat,role,date_adhesion
        $stmt->bind_result($idUtilisteur,$login,$pwd,$telephone,$name,$role,$id_bureau,$bureau,$centre);
        //Check for results
        
        if($stmt->fetch()){
            //Populate the movie array
                $userArray["idUtilisteur"] = $idUtilisteur;
				$userArray["login"] = $login;
				$userArray["pwd"] = $pwd;
				$userArray["telephone"] = $telephone;
				$userArray["name"] = $name;
				$userArray["role"] = $role;
				$userArray["id_bureau"] = $id_bureau;
				$userArray["bureau"] = $bureau;
				$userArray["centre"] = $centre;

				$response["success"] = 1;
				$response["message"] ="Bienvenu $name";
				$response["data"] = $userArray;




        }else{
            //When movie is not found
            $response["success"] = 0;
            $response["message"] = "Identifiant ou mot de passe incorrect !";
            $response["data"] = $userArray;
        }
        $stmt->close();


    }else{
        //Whe some error occurs
        $response["success"] = 0;
        $response["message"] = "Erreur de système";
        $response["data"] = $userArray;

    }

}else{
    //When the mandatory parameter movie_id is missing
    $response["status"] = 0;
    $response["message"] = "Parametre manquant";
    $response["data"] = $userArray;
}
//Display JSON response
echo json_encode($response);
?>