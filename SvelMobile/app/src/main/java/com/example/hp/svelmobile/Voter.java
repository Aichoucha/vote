package com.example.hp.svelmobile;

import android.app.ActivityManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.example.hp.svelmobile.helper.BackgroundService;
import com.example.hp.svelmobile.helper.ConfigHelper;
import com.example.hp.svelmobile.helper.HttpJsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import static com.example.hp.svelmobile.helper.BackgroundService.*;
import static com.example.hp.svelmobile.helper.RSAUtil.encrypt;
import static com.example.hp.svelmobile.helper.User.mediaPlayer;
public class Voter extends AppCompatActivity {
    private String id_Electeur,numElecteur,numMatricule,nom,prenom,date_naissance,lieu_naissance,pays_nationalite,parcours,statut,genre,etat,langue,partie,candidat,photo,e_id_bureau,e_bureau,e_centre;
    private int success;
    private String message,bureau;
    private ProgressDialog pDialog;
    private Button btnValid,btnAnnuler,button4;
    private ImageView imgView;

    private static final String KEY_SUCCESS = "success";
    private static final String KEY_DATA = "data";
    private static final String KEY_BUREAU = "bureau";
    private static final String KEY_IDCANDIDAT = "id_Candidat";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_NOM = "nom";
    private static final String KEY_PRENNOM = "prenom";
    private static final String KEY_GENREE= "genre";
    private static final String KEY_PARTI= "sigle_parti";
    private static final String KEY_PHOTO= "photo";
    private static final String KEY_DATE= "date";
    private static final String KEY_IDELECTEUR= "id_Electeur";

    String API_URL;
    String BASE_URL;




    Intent mServiceIntent;
    Context ctx;
    public Context getCtx() {
        return ctx;
    }

    int count=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voter);
        API_URL = ConfigHelper.getConfigValue(this,"api_url");
        BASE_URL = API_URL+"Vote.php";


        ctx=this;
        Intent curUser = getIntent();
        langue=curUser.getStringExtra("langue");

        switch(langue){
            case "bambara":
                bambaraLangue();
                break;
            case  "francais":
                francaisLangue();
                break;
        }

        id_Electeur=curUser.getStringExtra("idElecteur");
        numElecteur=curUser.getStringExtra("numElecteur");
        numMatricule=curUser.getStringExtra("numMatricule");
        nom=curUser.getStringExtra("nom");
        prenom=curUser.getStringExtra("prenom");
        date_naissance=curUser.getStringExtra("date_naissance");
        lieu_naissance=curUser.getStringExtra("lieu_naissance");
        pays_nationalite=curUser.getStringExtra("pays_nationalite");
        parcours=curUser.getStringExtra("parcours");
        statut=curUser.getStringExtra("statut");
        genre=curUser.getStringExtra("genre");
        etat=curUser.getStringExtra("etat");

        e_id_bureau=curUser.getStringExtra("id_bureau");
        e_bureau=curUser.getStringExtra("bureau");
        e_centre=curUser.getStringExtra("centre");


        candidat=curUser.getStringExtra("candidat");
        partie=curUser.getStringExtra("partie");
        photo=curUser.getStringExtra("photo");

        btnValid=findViewById(R.id.button);
        btnAnnuler=findViewById(R.id.annuler);
        button4=findViewById(R.id.button4);
        imgView=findViewById(R.id.imageView);

        setTitle("SVEL - "+prenom+" "+nom+" => "+partie);

        showCandidatSelected(); // show candidat


        btnValid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                if(count==1){
                    new AddVoteAsyncTask().execute();
                }else{
                Toast.makeText(Voter.this,"SVP, votre vote est en cours, veuillez patienter !", Toast.LENGTH_LONG).show();
                }

            }
        });
        imgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                new AddVoteAsyncTask().execute();
            }
        });

        btnAnnuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mediaPlayer.isPlaying()){
                    mediaPlayer.stop();
                }


                Toast.makeText(Voter.this,"Vous avez annulé votre choix de candidat!", Toast.LENGTH_LONG).show();
                Intent i =new Intent(Voter.this,VoteActivity.class);
                i.putExtra("langue",langue);
                i.putExtra("idElecteur",id_Electeur);
                i.putExtra("numElecteur",numElecteur);
                i.putExtra("numMatricule",numMatricule);
                i.putExtra("nom",nom);
                i.putExtra("prenom",prenom);
                i.putExtra("date_naissance",date_naissance);
                i.putExtra("lieu_naissance",lieu_naissance);
                i.putExtra("pays_nationalite",pays_nationalite);
                i.putExtra("statut",statut);
                i.putExtra("parcours",parcours);
                i.putExtra("genre",genre);
                i.putExtra("etat",etat);

                i.putExtra("id_bureau",e_id_bureau);
                i.putExtra("centre",e_bureau);
                i.putExtra("centre",e_centre);

                startActivity(i);
                finish();
            }
        });


    }


    public void showCandidatSelected(){
        imgView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imgView.setAdjustViewBounds(true);
        button4.setText("Votez pour "+partie+" ?");
        btnValid.setText("OUI");
        btnAnnuler.setText("NON");
        new DownloadImageTask((ImageView)findViewById(R.id.imageView)).execute(photo);
//        imgView.setImageBitmap(BitmapFactory.decodeStream(new ByteArrayInputStream(Base64.decode(photo.getBytes(), 0))));
    }

    private class AddVoteAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //Display proggress bar
            pDialog = new ProgressDialog(Voter.this);
            pDialog.setMessage("Vote en cours...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        protected String doInBackground(String... params) {
            HttpJsonParser httpJsonParser = new HttpJsonParser();
            Map<String, String> httpParams = new HashMap<>();
            //Populating request parameters

            try {
                String partie_x = Base64.encodeToString(encrypt(ctx,partie),Base64.DEFAULT);
                //String id_Electeur_x = Base64.encodeToString(encrypt(ctx,id_Electeur),Base64.DEFAULT);

            Log.d("RSA",KEY_IDELECTEUR+" : "+id_Electeur);
            Log.d("RSA",KEY_IDELECTEUR+" : "+id_Electeur);
            Log.d("RSA",KEY_PARTI+" : "+partie_x);

            httpParams.put("id_Electeur_x", id_Electeur);
            httpParams.put(KEY_IDELECTEUR, id_Electeur);
            httpParams.put(KEY_PARTI, partie_x);

            httpParams.put("bureau", e_id_bureau);

            } catch (Exception e) {
                e.printStackTrace();
            }



            JSONObject jsonObject = httpJsonParser.makeHttpRequest(BASE_URL, "POST", httpParams);

            try {
                success = jsonObject.getInt(KEY_SUCCESS);
                message = jsonObject.getString(KEY_MESSAGE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        protected void onPostExecute(String result) {
            pDialog.dismiss();
            runOnUiThread(new Runnable() {
                public void run() {
                    if (success == 1) {

                        isTimerRunning=false;

                        //Display success message
                        switch(langue){
                            case "bambara":
                                MerciebambaraLangue();
                                break;
                            case  "francais":
                                MercifrancaisLangue();
                                break;
                        }

                        Toast.makeText(Voter.this,message, Toast.LENGTH_LONG).show();

//                        Intent i = getIntent();
//                        InCommingActivity.setBoolRest(false);
//                        //send result code 20 to notify about movie update
//                        setResult(20, i);
//                        //Finish ths activity and go back to listing activity
                        Toast.makeText(Voter.this,"Merci pour votre citoyenneté !", Toast.LENGTH_LONG).show();
                        finish();

                    } else {
                        Toast.makeText(Voter.this,message, Toast.LENGTH_LONG).show();

                    }
                }
            });
        }
    }


    private void bambaraLangue(){
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer=MediaPlayer.create(this,R.raw.candichoisibamb);
            mediaPlayer.start();
        }else {
            mediaPlayer=MediaPlayer.create(this,R.raw.candichoisibamb);
            mediaPlayer.start();
        }


    }
    private void MerciebambaraLangue(){
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer=MediaPlayer.create(this,R.raw.mercibamb);
            mediaPlayer.start();
        }else {
            mediaPlayer=MediaPlayer.create(this,R.raw.mercibamb);
            mediaPlayer.start();
        }


    }
    private void francaisLangue(){
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer=MediaPlayer.create(this,R.raw.choicandifranc);
            mediaPlayer.start();
        }else {
            mediaPlayer=MediaPlayer.create(this,R.raw.choicandifranc);
            mediaPlayer.start();
        }
    }

    private void MercifrancaisLangue(){
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer=MediaPlayer.create(this,R.raw.mercifranc);
            mediaPlayer.start();
        }else {
            mediaPlayer=MediaPlayer.create(this,R.raw.mercifranc);
            mediaPlayer.start();
        }
    }

    public void timeResrtant_trois(){
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer=MediaPlayer.create(this,R.raw.franchoisi);
            mediaPlayer.start();
        }else {
            mediaPlayer=MediaPlayer.create(this,R.raw.franchoisi);
            mediaPlayer.start();
        }

    }
    public void timeResrtant_un(){
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
            mediaPlayer=MediaPlayer.create(this,R.raw.franchoisi);
            mediaPlayer.start();
        }else {
            mediaPlayer=MediaPlayer.create(this,R.raw.franchoisi);
            mediaPlayer.start();
        }

    }

    @Override
    public void onBackPressed() {
        if(mediaPlayer.isPlaying()){
            mediaPlayer.stop();
        }
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                Log.d("isMyServiceRunning?", true+"");
                return true;
            }
        }
        Log.d ("isMyServiceRunning?", false+"");
        return false;
    }
}
