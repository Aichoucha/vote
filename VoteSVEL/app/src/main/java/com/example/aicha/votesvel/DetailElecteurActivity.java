package com.example.aicha.votesvel;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.aicha.votesvel.helper.ConfigHelper;
import com.example.aicha.votesvel.helper.MySingleton;
import com.example.aicha.votesvel.helper.SessionHandler;
import com.example.aicha.votesvel.helper.User;

import org.json.JSONException;
import org.json.JSONObject;

public class DetailElecteurActivity extends AppCompatActivity {

    private static final String KEY_EMPTY = "";

    private static final String KEY_DATA = "data";
    private static final String KEY_MESSAGE = "message";
    private static final String KEY_SUCCESS = "success";
    private static final String KEY_ID_ELECTEUR = "id_Electeur";
    private static final String KEY_NUMELECTEUR = "numElecteur";
    private static final String KEY_NUMMATRICULE = "numMatricule";
    private static final String KEY_NOM = "nom";
    private static final String KEY_PRENOM = "prenom";
    private static final String KEY_DATE_NAISSANCE= "date_naissance";
    private static final String KEY_LIEU_NAISSANCE= "lieu_naissance";
    private static final String KEY_PAYS_NATIONALITE= "pays_nationalite";
    private static final String KEY_PARCOURS= "parcours";
    private static final String KEY_STATUT= "statut";
    private static final String KEY_GENRE= "genre";
    private static final String KEY_ETAT= "etat";

    String API_URL;
    String BASE_URL;

    private String id_Electeur;
    private String numElecteur;
    private String numMatricule;
    private String id_bureau;
    private String nom;
    private String prenom;
    private String date_naissance;
    private String lieu_naissance;
    private String pays_nationalite;
    private String parcours;
    private String statut;
    private String genre;
    private String etat;
    private String role;
    private String bureau;
    private String centre;

    private TextView txtName,txtNum,txtDate,txtLieu,txtGenre,txtEtat;

    private int success;
    private String message,txtNumElecteur;
    private ProgressDialog pDialog;
    String numero;
    private SessionHandler session;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_electeur);

        API_URL = ConfigHelper.getConfigValue(this,"api_url");
        BASE_URL = API_URL+"getDetailElecteur.php";

        session = new SessionHandler(getApplicationContext());
        user=session.getUserDetails();
        role=user.getRole();

        txtName=findViewById(R.id.txtName);
        txtNum=findViewById(R.id.txtNum);
        txtDate=findViewById(R.id.txtDate);
        txtLieu=findViewById(R.id.txtLieu);
        txtGenre=findViewById(R.id.txtGenre);
        txtEtat=findViewById(R.id.txtEtat);

        Intent intent=getIntent();
        numero=intent.getStringExtra("numero");

        loadElecteur();
        setTitle("Détail ");
    }


    private void displayLoader() {
        pDialog = new ProgressDialog(DetailElecteurActivity.this);
        pDialog.setMessage("Finding ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void loadElecteur(){
        displayLoader();
        JSONObject request = new JSONObject();
        try {
            //Populate the request parameters
            request.put(KEY_NUMELECTEUR, numero);
            Log.d("GETTING ELECTEUR","Numero electeur : "+txtNumElecteur);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsArrayRequest = new JsonObjectRequest
                (Request.Method.POST, BASE_URL, request, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        pDialog.dismiss();
                        try {
                            //Check if master got logged in successfully
                            message=response.getString(KEY_MESSAGE);

                            if (response.getInt(KEY_SUCCESS) == 1) {
                                JSONObject jsonObject=response.getJSONObject(KEY_DATA);

                                id_Electeur = jsonObject.getString("id_Electeur");
                                numElecteur = jsonObject.getString("numElecteur");
                                numMatricule = jsonObject.getString("numMatricule");
                                nom = jsonObject.getString("nom");
                                prenom = jsonObject.getString("prenom");
                                date_naissance = jsonObject.getString("date_naissance");
                                lieu_naissance = jsonObject.getString("lieu_naissance");
                                pays_nationalite = jsonObject.getString("pays_nationalite");
                                parcours = jsonObject.getString("parcours");
                                statut = jsonObject.getString("statut");
                                genre = jsonObject.getString("genre");
                                etat = jsonObject.getString("etat");
                                id_bureau = jsonObject.getString("id_bureau");
                                bureau = jsonObject.getString("bureau");
                                centre = jsonObject.getString("centre");

                                txtName.setText(nom+" "+prenom);
                                txtNum.setText(numElecteur);
                                txtDate.setText(date_naissance);
                                txtLieu.setText(lieu_naissance);
                                txtGenre.setText(genre);
                                txtEtat.setText(etat);

                            }else{

                                Toast.makeText(getApplicationContext(),message, Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pDialog.dismiss();

                        //Display error message whenever an error occurs
                        Toast.makeText(getApplicationContext(),error.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });

        // Access the RequestQueue through your singleton class.
        MySingleton.getInstance(this).addToRequestQueue(jsArrayRequest);
    }

}
