package com.example.aicha.votesvel;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.aicha.votesvel.helper.CheckNetworkStatus;
import com.example.aicha.votesvel.helper.SessionHandler;
import com.example.aicha.votesvel.helper.User;

public class AdminActivity extends AppCompatActivity {
 Button btnVerifier;
 Button btnVote;
 Button btnNonVote;
 Button btnResultat;
 TextView txtBureau;
    private SessionHandler session;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);


        session = new SessionHandler(getApplicationContext());
        user = session.getUserDetails();

        setTitle("Accueil - SVEL | "+user.getName());

        txtBureau=findViewById(R.id.txtBureau);
        txtBureau.setText(user.getCentre()+"=>"+user.getBureau());

        btnVerifier=findViewById(R.id.btnVerifier);
        btnVerifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {
//                    Intent veri = new Intent(AdminActivity.this, VerificationActivity.class);
//                    startActivity(veri);

                         new AlertDialog.Builder(AdminActivity.this)
                             .setTitle("Veuillez choisir le type de vérification")
                            .setMessage("Vérification d'electeur par " )
                            .setPositiveButton("Numéro", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent vote = new Intent(AdminActivity.this,VerificationActivity.class);
                                    startActivity(vote);
                                }
                            })
                            .setNegativeButton("Code QR", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent vote = new Intent(AdminActivity.this,Verification2Activity.class);
                                    startActivity(vote);
                                }
                            })
                            .show();


                }else{
                        Toast.makeText(AdminActivity.this,
                                "Impossible de se connecter à l'internet",
                                Toast.LENGTH_LONG).show();

                }

            }
        });

        btnVote = findViewById(R.id.btnVote);
        btnVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {
                    Intent vote = new Intent(AdminActivity.this,ListeVoteActivity.class);
                    startActivity(vote);
                }else{
                    Toast.makeText(AdminActivity.this,
                            "Impossible de se connecter à l'internet",
                            Toast.LENGTH_LONG).show();

                }

            }
        });

        btnNonVote = findViewById(R.id.btnNonVote);
        btnNonVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {
                    Intent nonVote = new Intent(AdminActivity.this,ListeNonVoteActivity.class);
                    startActivity(nonVote);
                }else{
                    Toast.makeText(AdminActivity.this,
                            "Impossible de se connecter à l'internet",
                            Toast.LENGTH_LONG).show();

                }

            }
        });

        btnResultat = findViewById(R.id.btnResultat);
        btnResultat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CheckNetworkStatus.isNetworkAvailable(getApplicationContext())) {
                    Intent result = new Intent(AdminActivity.this,ProvisoireActivity.class);
                    startActivity(result);
                }else{
                    Toast.makeText(AdminActivity.this,
                            "Impossible de se connecter à l'internet",
                            Toast.LENGTH_LONG).show();

                }

            }
        });
    }
}
