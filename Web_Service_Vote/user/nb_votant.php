<?php
/**
 * Created by PhpStorm.
 * User: AICHA
 * Date: 01/01/2018
 * Time: 03:30
 */

include '../db/db_connect.php';
//Query to select movie id and movie name
$result = array();
$electeurArray = array();
$response = array();

//Get the input request parameters

//Check for mandatory parameter
if(isset($_GET['centre']) && isset($_GET['bureau'])){

    $centre =$_GET['centre'];
    $bureau =$_GET['bureau'];


    $query_votant = "SELECT count(v.id_Electeur) as nb_votant
    FROM electeur e, bureau b ,centre c, vote v
    WHERE e.id_bureau=b.id  and b.centre_id=c.id AND e.id_Electeur=v.id_Electeur AND c.nom_centre='$centre' AND b.nom_bureau='$bureau'
    GROUP BY e.id_bureau";

    if($stmt=$con->prepare($query_votant)){
//        $stmt->bind_param("ss",$centre,$bureau);
        $stmt->execute();
        $stmt->bind_result($centre,$bureau,$nb_votant);

        while($stmt->fetch()){
            //Populate the movie array
            $electeurArray["centre"] = $centre;
            $electeurArray["bureau"] = $bureau;
            $electeurArray["nb_votant"] = $nb_votant;

            $result[]=$electeurArray;

        }

        $response["success"] = 1;
        $response["data"] = $result;
        $response["message"] = "En cours de Listing";
        $stmt->close();


    }else{
        //Some error while fetching data
        $response["success"] = 0;
        $response["data"] = $result;
        $response["message"] ="Erreur de serveur";

    }


}else{
    //When the mandatory parameter movie_id is missing
    $response["status"] = 0;
    $response["data"] = $result;
    $response["message"] = "Parametre manquant";

}
//Display JSON response
echo json_encode($response);
//var_dump($response);
?>