package com.example.hp.svelmobile.helper;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.example.hp.svelmobile.InCommingActivity;
import com.example.hp.svelmobile.R;
import com.example.hp.svelmobile.Voter;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.hp.svelmobile.helper.User.mediaPlayer;

public class BackgroundService extends Service {

    SessionHandler session;
    User user;
    public static int counter=0;

    Voter voter;

    // Start and end times in milliseconds
    private long startTime, endTime;

    // Is the service tracking time?
    public static boolean isTimerRunning;

    public static boolean isIsTimerRunning() {
        return isTimerRunning;
    }

    public static void setIsTimerRunning(boolean isTimerRunning) {
        isTimerRunning = isTimerRunning;
    }



    public BackgroundService(Context applicationContext) {
        super();
        Log.d("SERVICE", "SERVICE RUNNING!");

        session=new SessionHandler(applicationContext.getApplicationContext());
        user=session.getUserDetails();

    }

    public BackgroundService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        isTimerRunning=true;

        startTimer();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("EXIT", "ondestroy!");

        isTimerRunning=false;
        Intent broadcastIntent = new Intent(getApplicationContext(), InCommingActivity.class);

        startActivity(broadcastIntent);
        stoptimertask();
    }

    private static Timer timer;
    private TimerTask timerTask;
    long oldTime=0;
    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 1000, 1000); // 5 minute
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        Toast.makeText(getApplicationContext(),"LE SERVICE DE VOTE A DEMARRE", Toast.LENGTH_LONG).show();
        timerTask = new TimerTask() {
            public void run() {
                Log.d("timer", "timer : "+ (counter++));

                if(isTimerRunning){

                    if(counter == 60){ //120
                        mediaPlayer.stop();
                        mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.trois);
                        mediaPlayer.start();
                        Log.d("timer", "timer : Il vous reste 3 minutes ");
                    }else if(counter == 120){
                        mediaPlayer.stop();
                        mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.deux);
                        mediaPlayer.start();
                        Log.d("timer", "timer : Il vous reste 2 minutes ");
                    }else if(counter == 180){
                        Log.d("timer", "timer : Il vous reste 1 minute ");
                        mediaPlayer.stop();
                        mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.une);
                        mediaPlayer.start();
                    }else if(counter == 240){
                        mediaPlayer.stop();
                        mediaPlayer=MediaPlayer.create(getApplicationContext(),R.raw.fini);
                        mediaPlayer.start();
                        Log.d("timer", "timer : finis");

                        onDestroy();

                    }

                }else {

                    onDestroy();

                }


            }
        };
    }

    /**
     * not needed
     */
    public static void stoptimertask() {
        //stop the timer, if it's not already nul

        if (timer != null) {
            timer.cancel();
            timer = null;
            counter=0;
            mediaPlayer=null;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}