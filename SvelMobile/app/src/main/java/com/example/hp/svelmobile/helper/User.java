package com.example.hp.svelmobile.helper;
import android.media.MediaPlayer;

import java.util.Date;

public class User {


    String idUtilisteur,login,pwd,telephone,name,role,id_bureau,bureau,centre ;

    Date sessionExpiryDate;

    public static MediaPlayer mediaPlayer;

    public void setSessionExpiryDate(Date sessionExpiryDate) {
        this.sessionExpiryDate = sessionExpiryDate;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getSessionExpiryDate() {
        return sessionExpiryDate;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getIdUtilisteur() {
        return idUtilisteur;
    }

    public void setIdUtilisteur(String idUtilisteur) {
        this.idUtilisteur = idUtilisteur;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getId_bureau() {
        return id_bureau;
    }

    public void setId_bureau(String id_bureau) {
        this.id_bureau = id_bureau;
    }

    public String getBureau() {
        return bureau;
    }

    public void setBureau(String bureau) {
        this.bureau = bureau;
    }

    public String getCentre() {
        return centre;
    }

    public void setCentre(String centre) {
        this.centre = centre;
    }
}
