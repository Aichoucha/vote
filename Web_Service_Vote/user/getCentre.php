<?php
/**
 * Created by PhpStorm.
 * User: AICHA
 * Date: 01/01/2018
 * Time: 03:30
 */

include '../db/db_connect.php';
//Query to select movie id and movie name
$result = array();
$electeurArray = array();
$response = array();

//Get the input request parameters

//Check for mandatory parameter
if(isset($_GET['mmesamake'])){



    $query = "SELECT * FROM centre ";

    if($stmt=$con->prepare($query)){
//        $stmt->bind_param("ss",$centre,$bureau);
        $stmt->execute();
        $stmt->bind_result($id,$nom_centre);

        while($stmt->fetch()){
            //Populate the movie array
			$electeurArray["id"] = $id;
            $electeurArray["nom_centre"] = $nom_centre;


            $result[]=$electeurArray;

        }

        $response["success"] = 1;
        $response["data"] = $result;
        $response["message"] = "";
        $stmt->close();


    }else{
        //Some error while fetching data
        $response["success"] = 0;
        $response["data"] = $result;
        $response["message"] ="Erreur de serveur";

    }


}else{
    //When the mandatory parameter movie_id is missing
    $response["status"] = 0;
    $response["data"] = $result;
    $response["message"] = "Parametre manquant";

}
//Display JSON response
echo json_encode($response);
//var_dump($response);
?>