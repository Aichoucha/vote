<?php
/**
 * Created by PhpStorm.
 * User: AICHA
 * Date: 01/01/2018
 * Time: 03:30
 */

include '../db/db_connect.php';
//Query to select movie id and movie name
$result = array();
$electeurArray = array();
$response = array();

//Get the input request parameters

//Check for mandatory parameter
if(isset($_GET['bureau']) && isset($_GET['centre'])){

$centre =$_GET['centre'];
$bureau =$_GET['bureau'];


$query = "SELECT 
v.id_Candidat as NumCandidat, 
count(v.id_Candidat) as NombreVotant, 
count(v.id_Candidat)*100/(select count(id_Electeur) from electeur) as Voix,
c.sigle_parti as parti,
c.photo_Candidat as photo,
c.nom as nom,
c.prenom as prenom,
b.nom_bureau as bureau, 
centre.nom_centre as centre 
from vote v,candidat c,bureau b,electeur e, centre 
WHERE 
centre.id=b.centre_id AND 
b.id=e.id_bureau AND 
e.id_Electeur=v.id_Electeur AND 
v.id_Candidat=c.id_Candidat AND 
c.id_Candidat=v.id_Candidat AND 
centre.nom_centre='$centre' AND b.nom_bureau='$bureau'
group by centre.nom_centre, b.id, v.id_Candidat 
ORDER BY centre,bureau,c.sigle_parti";

if($stmt=$con->prepare($query)){
	$stmt->execute();
	$stmt->bind_result($NumCandidat,$NombreVotant,$Voix,$parti,$photo,$nom,$prenom,$bureau,$centre);
	while($stmt->fetch()){
            //Populate the movie array
            $electeurArray["NumCandidat"] = $NumCandidat;
            $electeurArray["NombreVotant"] = $NombreVotant;
            $electeurArray["Voix"] = $Voix;
            $electeurArray["parti"] = $parti;
            $electeurArray["nom"] = $nom;
            $electeurArray["prenom"] = $prenom;
            $electeurArray["bureau"] = $bureau;
            $electeurArray["centre"] = $centre;

//            $electeurArray["photo_Candidat"] = $photo_Candidat;
//            $electeurArray["photo_Candidat"] = '<img src="data:image/jpeg;base64,'.base64_encode($photo_Candidat).'"/>';
            //$electeurArray["photo"] = base64_encode($photo);
        //http://localhost/web_Service_Vote/img
//             $electeurArray["photo"] = "http://10.0.2.2/Web_Service_Vote/img/".$photo;
             $electeurArray["photo"] = "http://www.maliafrique.ml/Web_Service_Vote/img/".$photo;

            $result[]=$electeurArray;
		
	}
        
	$response["success"] = 1;
	$response["data"] = $result;
	$response["message"] = "En cours de Listing";
//       var_dump($result);
//    exit();
        $stmt->close();
	

}else{
	//Some error while fetching data
	$response["success"] = 0;
        $response["data"] = $result;
	$response["message"] ="Erreur de serveur";
	
}


}else{
	//When the mandatory parameter movie_id is missing
	$response["status"] = 0;
        $response["data"] = $result;
	$response["message"] = "Parametre manquant";

}
//Display JSON response
echo json_encode($response);
//var_dump($response);
?>