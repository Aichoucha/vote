package com.example.aicha.votesvel;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;

public class GridViewAdapter extends BaseAdapter {

    private Context context;
   final  private String [] app_prenom;
   final  private String [] app_nb;
   final  private String [] app_name;
   final  private String [] app_partie;
    private int [] app_icon;
    private String[] app_input;
//    app_nb
    public GridViewAdapter(Context context, String [] app_prenom,String [] app_nb,String [] app_name, String [] app_partie, String[] app_input) {

        this.context=context;
        this.app_prenom=app_prenom;
        this.app_nb=app_nb;
        this.app_input=app_input;
        this.app_name=app_name;
        this.app_partie=app_partie;
    }

    @Override
    public int getCount() {
        return app_name.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView=inflater.inflate(R.layout.row_grid, null);


        final TextView namePrenom=(TextView)convertView.findViewById(R.id.textView1);
        namePrenom.setText(app_prenom[position]);

        final TextView name=(TextView)convertView.findViewById(R.id.textView);
        name.setText(app_name[position]);

        final TextView name_app_nb=(TextView)convertView.findViewById(R.id.textView3);
        name_app_nb.setText(app_nb[position]);

        final TextView partie=(TextView)convertView.findViewById(R.id.textView2);
        partie.setText(app_partie[position]);

        ImageView icon=convertView.findViewById(R.id.imageView);
//        icon.setImageResource(app_icon[position]);

//        if(position==0){
//            icon.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            icon.setAdjustViewBounds(true);
//            icon.setImageBitmap(BitmapFactory.decodeStream(app_input[0]));
//        }else{
//            icon.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            icon.setAdjustViewBounds(true);
//            icon.setImageBitmap(BitmapFactory.decodeStream(app_input[position]));
//        }

        new DownloadImageTask((ImageView)convertView.findViewById(R.id.imageView))
                .execute(app_input[position]);

        Log.d("position","position : "+position+" "+app_input[position]);


        return convertView;
    }

//    InputStream is; //stream pointing to your blob or file
//    imageView=new ImageView(this);
//    imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
//    imageView.setAdjustViewBounds(true);
//    imageView.setImageBitmap(BitmapFactory.decodeStream(is));


    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

}
